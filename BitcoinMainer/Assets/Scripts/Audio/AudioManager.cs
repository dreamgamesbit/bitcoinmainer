﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour {

    public Sound[] sounds;
    public  bool Offmusic = false;
    public bool Offsound = false;
    public Button Music;
    public Button Sound;
    public List<Sprite> Icon;

    // Use this for initialization
    void Awake () {


        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;

            s.source.loop = s.loop;

        }
	}
    private void Start()
    {
      Play("Main Sound");      
    }

    public void Play(string name)
    {
        if (!Offsound)
        {
            Sound s = Array.Find(sounds, sound => sound.Name == name);
            s.source.PlayOneShot(s.clip);
        }
    }

    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.Name == name);
        s.source.Stop();
    }


    public void OffSound()
    {
        if (Offsound)
        {
            Offsound = false;
            Sound.GetComponent<Image>().sprite = Icon[0]; 
        }
        else
        {
            Offsound = true;
            Sound.GetComponent<Image>().sprite = Icon[1];
        }
    }

    public void OffMusic()
    {
        if (Offmusic)
        {
            Sound s = Array.Find(sounds, sound => sound.Name == "Main Sound");
            s.source.volume = 0.005f;
            Offmusic = false;
            Music.GetComponent<Image>().sprite = Icon[2];
        }
        else
        {
            Sound s = Array.Find(sounds, sound => sound.Name == "Main Sound");
            s.source.volume = 0;
            Offmusic = true;
            Music.GetComponent<Image>().sprite = Icon[3];
        }
    }
    
   
}
