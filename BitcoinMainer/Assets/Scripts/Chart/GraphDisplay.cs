﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Manager.GameManager;

//Отображение и создание линий графика валют
public class GraphDisplay : MonoBehaviour
{

    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public Currency CurrencyRef;
        public int size;
       

        public Pool(string _tag, GameObject _prefab, int _size, Currency _CurrencyRef)
        {
            tag = _tag;
            prefab = _prefab;
            size = _size;
            CurrencyRef = _CurrencyRef;
        }
    }


    List<Pool> pools;
    Dictionary<string, Queue<GameObject>> poolDictionary;
    GameObject LastPoint = null; // NextPoint = null;
    [SerializeField]
    int SizePool;
    Transform TargetSpawnPoint;
    [SerializeField]
    Image BackGround;
    [SerializeField]
    Text TimeLeftDisplay;


    public Image GetBackGround { get => BackGround; private set => BackGround = value; }
    public Dictionary<string, Queue<GameObject>> GetpoolDictionary { get => poolDictionary; private set => poolDictionary = value; }


    void Start()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;
        BackGround.sprite = GetCurentDevice.GetWallPaper;
    }

    private void Update()
    {
        TimeLeftDisplay.text = ((int)(GetGameManager.GetTimeLeft/60)).ToString()+" : "+(GetGameManager.GetTimeLeft % 60).ToString();
    }

    public void Prime(List<Currency> AviableCUrrency)
    {
        TargetSpawnPoint = GameObject.Find("graphContainer").transform;
        if (TargetSpawnPoint.childCount > 0)
        {
            for (int i = 0; i < TargetSpawnPoint.childCount; i++)
            {
                Destroy(TargetSpawnPoint.GetChild(i).gameObject);
            }

        }
        SetPoolItems(AviableCUrrency);

        poolDictionary = new Dictionary<string, Queue<GameObject>>();
        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();
            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.transform.SetParent(TargetSpawnPoint, false);
                obj.GetComponent<Image>().color = pool.CurrencyRef.GetColor; 
                obj.transform.GetChild(0).GetComponent<Image>().color= pool.CurrencyRef.GetColor;
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }

            poolDictionary.Add(pool.tag, objectPool);
        }
    }

    private void SetPoolItems(List<Currency> List)
    {
        pools = new List<Pool>();

        for (int i = 0; i < List.Count; i++)
        {
            pools.Add(new Pool(List[i].GetNameCurrency, List[i].GetPoint,SizePool, List[i]));
        }
    }

    public GameObject SpawnFromPool(string tag, Vector2 position)
    {

        if (!poolDictionary.ContainsKey(tag))
        {
            Debug.LogWarning("Pool with tag" + tag + "doesn't exsist");
            return null;
        }

        
        GameObject objectToSpawn = poolDictionary[tag].Dequeue();
        GameObject nextObjectToSpawn = poolDictionary[tag].Peek();
              
        RectTransform recttransObj = objectToSpawn.GetComponent<RectTransform>();

        recttransObj.anchoredPosition = position;

        objectToSpawn.SetActive(true);


        IPooledObject pooledObj = objectToSpawn.GetComponent<IPooledObject>();
        IPooledObject nextpooledObj = nextObjectToSpawn.GetComponent<IPooledObject>();



        poolDictionary[tag].Enqueue(objectToSpawn);

        LastPoint = objectToSpawn;

        if (nextpooledObj != null)
        {
            nextpooledObj.OnObjectPosition(LastPoint);

        }

        if (pooledObj != null)
        {
            pooledObj.OnObjectSpawn(position);
            
        }
        

      
        return objectToSpawn;
    }

    public void Close()
    {
        FindObjectOfType<AudioManager>().Play("Click");
        GetGraph.StopAllCoroutines();
        GetMenu.gameObject.SetActive(true);
        gameObject.SetActive(false);
       
    }
    
}
