﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Manager.GameManager;

//Управление всем окном графика валют
public class Graph : MonoBehaviour {

    [SerializeField]    
    GameObject graphContainer;
    [SerializeField]
    GraphDisplay PrefabGraphDisplay;
    RectTransform graphWindow;
    Transform spawnPosition;
    internal bool Switchdevice = false;
    //GameObject Last = null;

    //float min = 0f, max = 0f;

    #region Internals
    public GraphDisplay GetGraphDisplay { get; private set; }
    #endregion

    public void Start()
    {
        //Initialize displlay graph and initialize curve all Availables Currency
        GetGraphDisplay = Instantiate(PrefabGraphDisplay);
        GetGraphDisplay.Prime(GetCurentDevice.GetAvailablesCurrency);
        GetGraphDisplay.GetComponent<ListCurrencyDisplay>().Prime(GetCurentDevice.GetAvailablesCurrency);
        GetGraphDisplay.gameObject.SetActive(false);
    }

    public void OpenGraph()
    {
        //Activate graph window/else create graph window
        if (GetGraphDisplay != null)
        {
            GetGraphDisplay.gameObject.SetActive(true);
            

            //If the device has not changed, continue to create points/else create new poll points of Availables Currency current device
            if (!Switchdevice)
            {
                StartCoroutine(SpawnPoint());
            }
            else
            {
                GetGraphDisplay.GetpoolDictionary.Clear();
                GetGraphDisplay.Prime(GetCurentDevice.GetAvailablesCurrency);
                GetGraphDisplay.GetComponent<ListCurrencyDisplay>().Prime(GetCurentDevice.GetAvailablesCurrency);                
                Switchdevice = false;
                StartCoroutine(SpawnPoint());
            }

        }
    }

    public void SwitchCurentCurrency(Currency ReferenceCurrencyPrefab)
    {
        GetGameManager.SetCurentCurrency(ReferenceCurrencyPrefab);
    }
       
    internal IEnumerator SpawnPoint()
    {
        int second = 0;
        System.Random Time = new System.Random();
        int TimeFluctuations = Time.Next(1,4);
        YieldInstruction YieldWaitInstruction = new WaitForSeconds(1f);
        while (true)
        {
            yield return YieldWaitInstruction;
            second++;
            if (second < TimeFluctuations)
            {
                //Creating a curve in graph
                for (int i = 0; i < GetCurentDevice.GetAvailablesCurrency.Count; i++)
                {
                    GetGraphDisplay.SpawnFromPool(GetCurentDevice.GetAvailablesCurrency[i].GetNameCurrency, new Vector2((graphContainer.GetComponent<RectTransform>().sizeDelta.x / 2)+60f , GetCurentDevice.GetAvailablesCurrency[i].GetRate/2-100));
                }
            }
            else
            {
                
                //Create flucruations 
                for (int i = 0; i < GetCurentDevice.GetAvailablesCurrency.Count; i++)
                {
                    GetCurentDevice.GetAvailablesCurrency[i].CalculatedFluctuations();
                    GetGraphDisplay.SpawnFromPool(GetCurentDevice.GetAvailablesCurrency[i].GetNameCurrency, new Vector2((graphContainer.GetComponent<RectTransform>().sizeDelta.x / 2)+60f, UnityEngine.Random.Range(GetCurentDevice.GetAvailablesCurrency[i].GetMinRate, GetCurentDevice.GetAvailablesCurrency[i].GetMaxRate)));
                }
                TimeFluctuations = Time.Next(1, 4);
                second = 0;
            }
          
        }
    }

}
