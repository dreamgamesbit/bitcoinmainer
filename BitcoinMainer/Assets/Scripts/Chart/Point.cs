﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Point : MonoBehaviour,IPooledObject {


    float min = 0f, max = 0f;
    Vector2 t;    
    GameObject Conected;
    GameObject LastPoint = null;
   
    public void OnObjectSpawn(Vector2 position)
    {

        //min = 0;
        //max = 0;
        t = position;

        if (LastPoint != null)
        {
            SpawnFromPoolConected(LastPoint.GetComponent<RectTransform>().anchoredPosition, GetComponent<RectTransform>().anchoredPosition, gameObject);
        }
        else
        {
           transform.Find("Conected").gameObject.SetActive(false);
        }


    }


    public void OnObjectPosition( GameObject Last)
    {
        LastPoint = Last;
    }

    public void Start()
    {
                
       GetComponent<RectTransform>().sizeDelta = new Vector2(11, 11);      
       
    }
    
    public void FixedUpdate()
    {
        this.GetComponent<RectTransform>().anchoredPosition = new Vector2(Mathf.Round(Mathf.Lerp(min, max, t.x)), t.y);

        t.x -= 50f * Time.deltaTime / 1f; ;

        min = max;
        max = t.x;
    }

    public void SpawnFromPoolConected(Vector2 pointPositionA, Vector2 pointPositionB, GameObject target)
    {

        GameObject dotConection = target.transform.Find("Conected").gameObject;
        dotConection.SetActive(true);
        dotConection.transform.SetParent(target.transform, false);
        RectTransform rectTransformPoint = dotConection.GetComponent<RectTransform>();
        Vector2 dir = (pointPositionB - pointPositionA).normalized;
        float distance = Vector2.Distance(pointPositionB, pointPositionA);
        rectTransformPoint.anchoredPosition = -dir * distance * .5f;
        rectTransformPoint.anchorMax = new Vector2(0.5f, 0.5f);
        rectTransformPoint.anchorMin = new Vector2(0.5f, 0.5f);
        rectTransformPoint.sizeDelta = new Vector2(distance, 3f);
        float sign = (pointPositionB.y < pointPositionA.y) ? -1.0f : 1.0f;
        rectTransformPoint.localEulerAngles = new Vector3(0, 0, Vector2.Angle(Vector2.right, dir) * sign);

    }

}
