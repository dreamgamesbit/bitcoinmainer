﻿
using UnityEngine;

public interface IPooledObject{

    void OnObjectSpawn(Vector2 position);
    void OnObjectPosition( GameObject LastPoint);
}
