﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Manager.GameManager;

public class Menu : MonoBehaviour {
    
    Button ShopButton;
    Button UpgradeButton;
    Button ChartButton;
    Button TaskButton;
    Button DonationButton;

    public GameObject GetMenuButton
    {
        get; private set;
    }

    public Image GetBackGround
    {
        get;private set;
    }



	// Use this for initialization
	void Start () {

        //Set world camera
        GetComponent<Canvas>().worldCamera = Camera.main;

        //Get menu button
        GetMenuButton = GameObject.FindGameObjectWithTag("MenuButton");

        //Add method Open Menu to button menu
        GetMenuButton.GetComponent<Button>().onClick.AddListener(OpenMenu);

        //Get windows control buttons 
        ShopButton = transform.Find("Shop").GetComponent<Button>();
        UpgradeButton = transform.Find("ButtonUpgrade").GetComponent<Button>();
        ChartButton = transform.Find("ButtonChart").GetComponent<Button>();
        TaskButton = transform.Find("TaskButton").GetComponent<Button>();
        DonationButton = transform.Find("DonationButton").GetComponent<Button>();

        //Add methods Open window and Open app device (diactivate menu) for each control button
        ShopButton.onClick.AddListener(delegate { GetShop.OpenShop(); FindObjectOfType<AudioManager>().Play("Click"); });
        ShopButton.onClick.AddListener(OpenAppDevice);
        UpgradeButton.onClick.AddListener(delegate { GetUpgrade.OpenUpgrade(); FindObjectOfType<AudioManager>().Play("Click"); });
        UpgradeButton.onClick.AddListener(OpenAppDevice);
        ChartButton.onClick.AddListener(delegate { GetGraph.OpenGraph(); FindObjectOfType<AudioManager>().Play("Click"); });
        ChartButton.onClick.AddListener(OpenAppDevice);
        TaskButton.onClick.AddListener(delegate { GetTaskManager.OpenTaskWindow(); FindObjectOfType<AudioManager>().Play("Click"); });
        TaskButton.onClick.AddListener(OpenAppDevice);
        DonationButton.onClick.AddListener(OpenAppDevice);

        //Get reference on image component at menu object
        GetBackGround = transform.Find("BackGround").GetComponent<Image>();

        //Set saved background current device
        GetBackGround.sprite = GetCurentDevice.GetWallPaper;

        //Diactivate men
        gameObject.SetActive(false);

    }


    public void OpenMenu()
    {
        //Sound Open menu
        FindObjectOfType<AudioManager>().Play("OpenMenu");

        //Deactivate menu button
        GetMenuButton.SetActive(false);

        //Activate menu
        gameObject.SetActive(true);
    }


    public void OpenAppDevice()
    {
        gameObject.SetActive(false);
    }

    public void CloseMenu()
    {
        //Sound close menu
        FindObjectOfType<AudioManager>().Play("CloseMenu");

        //Activate menu button
        GetMenuButton.SetActive(true);

        //Diactivate men
        gameObject.SetActive(false);
    }
}
