﻿using System;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Manager.GameManager;

public class Income : MonoBehaviour {
    [SerializeField]
    Text CoinCounter;
    [SerializeField]
    Text CoinCounterPersecond;
    [SerializeField]
    Text DonatCoinCounter;

    decimal Balance;
    int DonatBalance;
    SaveData Save;

    float min = 0f, max = 0f,t= 0,clickCoin = 0;

    #region Internals
    public decimal GetBalance { get => Balance; set { if (value >= 0) Balance = value; } }
    public int GetDonatBalance { get => DonatBalance; set { if (value >= 0) DonatBalance = value; } }
    #endregion

    void Start()
    {
        DisplayDonatCoin();

        //Start maining
        StartCoroutine("CoinPerSecond");        
    }


    void Update()
    {   
        //Display current balance
        CoinCounter.text = IndexCoinCounter() + " " + GetCurentCurrency.GetNameCurrency;

        //Display mining speed
        CoinCounterPersecond.text = ((int)((GetCurentDevice.GetIncomePerSecondDevice / GetCurentCurrency.GetRate) + (clickCoin / GetCurentCurrency.GetRate))).ToString() + " " + GetCurentCurrency.GetNameCurrency +"/SEC";
    }

    public void DisplayDonatCoin()
    {
        DonatCoinCounter.text = DonatBalance.ToString();
    }

    string IndexCoinCounter()
    {
        if (Balance >= 1000) return Math.Round(Balance / 1000, 2).ToString() + 'k';
        else return Balance.ToString();
    }

    public void BuyDonatCoin(int amount)
    {
        DonatBalance += amount;
        DonatCoinCounter.text = DonatBalance.ToString();
    }

    public void ClickCoin()
    {
        clickCoin += GetCurentDevice.GetIncomePerClickDevice;
        FindObjectOfType<AudioManager>().Play("Income");
    }

    public void Reward(float value)
    {
        clickCoin += value;
    }

    public void RewardCryptoCoin(int value)
    {
        DonatBalance += value;
        DonatCoinCounter.text = DonatBalance.ToString();
    }
  
    IEnumerator CoinPerSecond()
    {
        t = (float)Balance;
        while (true)
        {
            Balance = (decimal)Math.Round(Mathf.Lerp(min, max, t));
            t += (GetCurentDevice.GetIncomePerSecondDevice / GetCurentCurrency.GetRate) * Time.deltaTime / 1f; //if speed=2 then time has been slowly on two points             
            if (clickCoin > 0)
            {
                t += (clickCoin / GetCurentCurrency.GetRate) * Time.deltaTime / 1f;
                clickCoin -= clickCoin * Time.deltaTime / 1f;

            }
            else clickCoin = 0;
            min = max;
            max = t;

            yield return 0;
        }
        
    }
}
