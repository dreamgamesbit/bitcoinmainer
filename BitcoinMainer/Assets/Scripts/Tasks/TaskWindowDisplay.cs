﻿using System.Collections.Generic;
using UnityEngine;
using static Manager.GameManager;
using UnityEngine.UI;

public class TaskWindowDisplay : MonoBehaviour
{
    [SerializeField]
    GameObject TargetTransformTaskItems;
    [SerializeField]
    Image BackGround;
    [SerializeField]
    Text TimeLeftDisplay;
    [SerializeField]
    TaskDisplay prefabDisplayTask;


    List<TaskDisplay> DisplayTaskToActivate=new List<TaskDisplay>();

    public List<TaskDisplay> GetDisplayTaskToActivate
    {
        get => DisplayTaskToActivate;
    }


    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;

        //Set background upgrade window
        BackGround.sprite = GetCurentDevice.GetWallPaper;
    }

    void Update()
    {
        TimeLeftDisplay.text = ((int)(GetTaskManager.GetTimeLeft / 60)).ToString() + " : " + (GetTaskManager.GetTimeLeft % 60).ToString();
    }

    public void PrimeTaskItems(Task TaskItem)
    {
        //Prime Task
        TaskItem.Prime();

        //Inizialize display item Task
        DisplayTaskToActivate.Add(Instantiate(prefabDisplayTask));
        TaskItem.displayTaskItem = DisplayTaskToActivate[DisplayTaskToActivate.Count - 1];
        DisplayTaskToActivate[DisplayTaskToActivate.Count-1].transform.SetParent(TargetTransformTaskItems.transform, false);
        DisplayTaskToActivate[DisplayTaskToActivate.Count - 1].Prime(TaskItem);

        
    }




    public void CloseTaskWindow()
    {
        FindObjectOfType<AudioManager>().Play("Click");
        GetMenu.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}
