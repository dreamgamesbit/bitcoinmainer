﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using static Manager.GameManager;
using UnityEngine.UI;
using UnityEngine;

public class TaskManger : MonoBehaviour
{
    [SerializeField]    
    List<Task> taskPool;
    [SerializeField]
    GameObject targetSpanwTask;
    List<Task> TaskToActivate;
    List<Task> SavedTasks = new List<Task>();

    float Timeleft;
    TimeSpan timeChangeInterval, nextTimeChange;

    [SerializeField]
    TaskWindowDisplay TaskWindowDisplay;

    SaveData Save;

    #region Internals
    public TaskWindowDisplay GetTaskWindowDisplay { get; private set; }

    public List<Task> GetAllTask
    {
        get => TaskToActivate;
    }

    public List<Task> GetSavedTasks
    {
        get => SavedTasks; set => SavedTasks = value;
    }

    public Task CurentTask
    {
        get; set;
    } = null;

    public float GetTimeLeft
    {
        get => Timeleft;
    }
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        
        Save = GetGameManager.GetSave;

        //load tasks
        if (Save.AllTasks != null)
        {
            
            for (int i = 0; i < Save.AllTasks.Length; i++)
            {
                string[] task = Save.AllTasks[i].Split('|');
                switch (task[0])
                {
                    case "Click Task":
                        SavedTasks.Add(new ClickT());
                        SavedTasks[i].NameTask = task[0];
                        SavedTasks[i].target = int.Parse(task[1]);
                        SavedTasks[i].progress = int.Parse(task[2]);
                        SavedTasks[i].reward = int.Parse(task[3]);
                        SavedTasks[i].readyFA = bool.Parse(task[4]);
                        break;
                    case "Balance Task":
                        SavedTasks.Add(new BalanceT());
                        SavedTasks[i].NameTask = task[0];
                        SavedTasks[i].target = int.Parse(task[1]);
                        SavedTasks[i].progress = int.Parse(task[2]);
                        SavedTasks[i].reward = int.Parse(task[3]);
                        SavedTasks[i].readyFA = bool.Parse(task[4]);
                        break;
                    case "Pasive Income Task":
                        SavedTasks.Add(new PasiveIncomeT());
                        SavedTasks[i].NameTask = task[0];
                        SavedTasks[i].target = int.Parse(task[1]);
                        SavedTasks[i].progress = int.Parse(task[2]);
                        SavedTasks[i].reward = int.Parse(task[3]);
                        SavedTasks[i].readyFA = bool.Parse(task[4]);
                        break;
                    default:
                        Debug.Log("Такого таска нет");
                        break;

                }
            }
        }


        TaskToActivate =new List<Task>();

        GetTaskWindowDisplay = Instantiate(TaskWindowDisplay);

        //Initialization Save Tasks
        InitializationSaveTasks();

        GetTaskWindowDisplay.gameObject.SetActive(false);

        #region TimerCalculate

        timeChangeInterval = new TimeSpan(0, 30, 00);

        if (Save.dataAddNewTask == null)
        {
            nextTimeChange = timeChangeInterval + DateTime.Now.TimeOfDay;

            Save.dataAddNewTask = new int[]
            {
                    DateTime.Now.Year,
                    DateTime.Now.Month,
                    DateTime.Now.Day,
                    nextTimeChange.Hours,
                    nextTimeChange.Minutes,
                    nextTimeChange.Seconds,
                    nextTimeChange.Milliseconds,

            };
        }
        else
        {
            DateTime savedDataAddNewTask = new DateTime(

                Save.dataAddNewTask[0],
                Save.dataAddNewTask[1],
                Save.dataAddNewTask[2],
                Save.dataAddNewTask[3],
                Save.dataAddNewTask[4],
                Save.dataAddNewTask[5],
                Save.dataAddNewTask[6]

                );

            TimeSpan checkdate = savedDataAddNewTask - DateTime.Now;

            if (checkdate.TotalSeconds > 0)
            {
                nextTimeChange = savedDataAddNewTask.TimeOfDay;
            }
            else
            {
                nextTimeChange = timeChangeInterval + DateTime.Now.TimeOfDay;

                Save.dataAddNewTask = new int[]
                {
                        DateTime.Now.Year,
                        DateTime.Now.Month,
                        DateTime.Now.Day,
                        nextTimeChange.Hours,
                        nextTimeChange.Minutes,
                        nextTimeChange.Seconds,
                        nextTimeChange.Milliseconds,

                };
                AddNewTask();

            }            
        }
        Timeleft = (float)(nextTimeChange - DateTime.Now.TimeOfDay).TotalSeconds;

        #endregion

        StartCoroutine("TimeAddTask");
    }

   public void AddNewTask()
    {
        for (int i = 0; i < taskPool.Count; i++)
        {

            int finde = (from c in TaskToActivate
                         where String.Equals(c.NameTask, taskPool[i].NameTask)
                         select c).Count();
            if (finde > 0)
            {
                continue;
            }
            else
            {
                TaskToActivate.Add(Instantiate(taskPool[i],targetSpanwTask.transform));
                GetTaskWindowDisplay.PrimeTaskItems(TaskToActivate[TaskToActivate.Count - 1]);               
                return;
            }
        }
           
           
        
    }

    public void DeleteTask(Task delTask)
    {
        TaskToActivate.Remove(delTask);
        foreach(Task t in TaskToActivate)
        {
            Debug.Log(t.NameTask);
        }
        
    }

    void InitializationSaveTasks()
    {
        if (SavedTasks != null)
        {
            for (int i = 0; i < SavedTasks.Count; i++)
            {
                if (SavedTasks[i].readyFA)
                {
                    switch (SavedTasks[i].NameTask)
                    {
                        case "Click Task":
                            TaskToActivate.Add(Instantiate(taskPool.Find(t => t.NameTask == "Click Task"), targetSpanwTask.transform));
                            TaskToActivate[TaskToActivate.Count - 1].target = SavedTasks[i].target;
                            TaskToActivate[TaskToActivate.Count - 1].progress = SavedTasks[i].progress;
                            TaskToActivate[TaskToActivate.Count - 1].reward = SavedTasks[i].reward;
                            GetTaskWindowDisplay.PrimeTaskItems(TaskToActivate[TaskToActivate.Count - 1]);
                            break;
                        case "Balance Task":
                            TaskToActivate.Add(Instantiate(taskPool.Find(t => t.NameTask == "Balance Task"), targetSpanwTask.transform));
                            TaskToActivate[TaskToActivate.Count - 1].target = SavedTasks[i].target;
                            TaskToActivate[TaskToActivate.Count - 1].progress = SavedTasks[i].progress;
                            TaskToActivate[TaskToActivate.Count - 1].reward = SavedTasks[i].reward;
                            GetTaskWindowDisplay.PrimeTaskItems(TaskToActivate[TaskToActivate.Count - 1]);
                            break;
                        case "Pasive Income Task":
                            TaskToActivate.Add(Instantiate(taskPool.Find(t => t.NameTask == "Pasive Income Task"), targetSpanwTask.transform));
                            TaskToActivate[TaskToActivate.Count - 1].target = SavedTasks[i].target;
                            TaskToActivate[TaskToActivate.Count - 1].progress = SavedTasks[i].progress;
                            TaskToActivate[TaskToActivate.Count - 1].reward = SavedTasks[i].reward;
                            GetTaskWindowDisplay.PrimeTaskItems(TaskToActivate[TaskToActivate.Count - 1]);
                            break;
                        default:
                            Debug.Log("Такого таска нет");
                            break;

                    }
                }

            }
        }

        if (Save.CurrentTask!=null && Save.CurrentTask !="")
        {
            BlockUIOtherTasks(TaskToActivate.Find(n => n.NameTask == Save.CurrentTask));            
            CurentTask.Activate();
        }
    }

    public void BlockUIOtherTasks(Task curentT)
    {
        CurentTask = curentT;
        if (CurentTask != null)
        {
            foreach (Task t in TaskToActivate)
            {
                t.displayTaskItem.DeactivateButton();
            }

            CurentTask.displayTaskItem.GetButtonActivate.gameObject.SetActive(false);
            CurentTask.displayTaskItem.GetProgressTask.gameObject.SetActive(true);
        }
    }

    public void ActiveUIOtherTasks()
    {
        CurentTask = null;

        foreach (Task t in TaskToActivate)
        {
            t.displayTaskItem.ActivateButton();
        }
    }

    public void OpenTaskWindow()
    {
        //Activate Upgrade Window
        GetTaskWindowDisplay.gameObject.SetActive(true);
    }


    IEnumerator TimeAddTask()
    {
        float t = Timeleft;
        while (true)
        {
            Timeleft = (float)Math.Round(t);
            if (t > 0)
            {
                t -= 1 * Time.deltaTime / 1f;
            }
            else
            {
                AddNewTask();

                nextTimeChange = timeChangeInterval + DateTime.Now.TimeOfDay;

                Save.dataAddNewTask = new int[]
                {
                    DateTime.Now.Year,
                    DateTime.Now.Month,
                    DateTime.Now.Day,
                    nextTimeChange.Hours,
                    nextTimeChange.Minutes,
                    nextTimeChange.Seconds,
                    nextTimeChange.Milliseconds,

                };
                t = (float)(nextTimeChange - DateTime.Now.TimeOfDay).TotalSeconds;
            }


            yield return 0;
        }

    }
}
