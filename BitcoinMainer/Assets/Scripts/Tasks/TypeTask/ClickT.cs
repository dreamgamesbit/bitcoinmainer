﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using static Manager.GameManager;



public sealed class ClickT:Task
{
    GameObject mainCanvas;

    public override string NameTask { get; set; } = "Click Task";

    public override void Prime()
    {
        readyFA = true;
        LoadProgress();
        int finde = (from c in GetTaskManager.GetSavedTasks
                     where c is ClickT
                     select c).Count();
        if (finde == 0)
        {
            GetTaskManager.GetSavedTasks.Add(new ClickT());
        }
    }

    public override void Activate()
    {
        mainCanvas = GameObject.FindGameObjectWithTag("MainCanvas");
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener(delegate { Performance(); });
        mainCanvas.GetComponent<EventTrigger>().triggers.Add(entry);
        GetTaskManager.BlockUIOtherTasks(this);
       // Debug.Log($"Задание сделать {target} кликов.");
    }

    public override void LoadProgress()
    {
        if (GetTaskManager.GetSavedTasks != null)
        {
           
            IEnumerable<Task> st = from s in GetTaskManager.GetSavedTasks
                                   where s is ClickT
                                   select s;

            if (st != null && st.Count()>0)
            {                
                foreach (Task item in st)
                {
                    if (item.target != 0)
                    {
                        
                        progress = item.progress;
                        target = item.target;
                        reward = item.reward;
                       // Debug.Log($"Прогресс загружен, кликов сделанно {progress} , награда {reward} {GetCurentCurrency.GetNameCurrency}");
                    }
                    else
                    {

                        CalculateReward();
                        CalculateTarget();

                       // Debug.Log($"Рассчитанно , цель {target} , награда {reward} {GetCurentCurrency.GetNameCurrency}");

                    }
                }              
                
            }
            else
            {

                CalculateReward();
                CalculateTarget();

               // Debug.Log($"Рассчитанно , цель {target} , награда {reward} {GetCurentCurrency.GetNameCurrency}");

            }
        }



    }

    public override void SaveProgress()
    {
        if (GetTaskManager.GetSavedTasks != null)
        {
            Task st = GetTaskManager.GetSavedTasks.Find(t => t.NameTask == "Click Task");
            st.progress = progress;
            st.target = target;
            st.reward = reward;
            st.readyFA = readyFA;
        }
    }

    public override void Deactivate()
    {
        readyFA = false;
        SaveProgress();        
        GetTaskManager.DeleteTask(this);
        Destroy(displayTaskItem.gameObject);
        GetTaskManager.ActiveUIOtherTasks();
        Destroy(this.gameObject);
    }

    public override void CalculateTarget()
    {
        if (progress == 0)
        {
            target = 10;
        }else
        {
            target *= 2;
        }
    }

    public override void CalculateReward()
    {
        if (reward == 0)
        {
            reward = 500;
        }
        else
        {
            reward = target * 10;
        }
    }

    public override void Performance()
    {
        progress++;
        displayTaskItem.GetProgressTask.text = "progress " + progress.ToString();
        if (progress >= target)
        {
            //Debug.Log($"Задание выполенно награда {reward } {GetCurentCurrency.GetNameCurrency}");
            GetIncome.Reward(reward * GetCurentCurrency.GetRate);
            CalculateTarget();
            CalculateReward();                     
            progress = 0;
            Deactivate();
        }
        //else
        //{
        //    Debug.Log($"Сделанно кликов {progress}");
        //}
        
        
    }

   


}
