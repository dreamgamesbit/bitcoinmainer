﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using static Manager.GameManager;
using UnityEngine;

public sealed class BalanceT : Task
{
    int StartBalance;

    public override string NameTask { get; set; } = "Balance Task";

    public override void Prime()
    {
        readyFA = true;
        LoadProgress();
        int finde = (from c in GetTaskManager.GetSavedTasks
                     where c is BalanceT
                     select c).Count();
        if (finde == 0)
        {
            GetTaskManager.GetSavedTasks.Add(new BalanceT());
        }
    }

    public override void Activate()
    {        
       if ((int)GetIncome.GetBalance == 0)
        {
            StartBalance = (int)GetGameManager.GetSave.Coin * (int)GetCurentCurrency.GetRate - progress;
        }
        else
        {
            StartBalance = (int)GetIncome.GetBalance * (int)GetCurentCurrency.GetRate - progress;
        }        
        GetTaskManager.BlockUIOtherTasks(this);
       
        StartCoroutine("CheckProgress");
       // Debug.Log($"Задание зработать {target } {GetCurentCurrency.GetNameCurrency}");
    }

    public override void LoadProgress()
    {
        if (GetTaskManager.GetSavedTasks != null)
        {
            IEnumerable<Task> st = from s in GetTaskManager.GetSavedTasks
                                   where s is BalanceT
                                   select s;

            if (st != null && st.Count() > 0)
            {
                foreach (Task item in st)
                {
                    if (item.target != 0)
                    {
                        progress = item.progress;
                        target = item.target;
                        reward = item.reward;
                    }
                    else
                    {

                        CalculateReward();
                        CalculateTarget();

                        //Debug.Log($"Рассчитанно, цель {target} , награда {reward} CryptoCoin");

                    }
                }
               // Debug.Log($"Прогресс загружен, balance {progress} , награда {reward} CryptoCoin");
            }
            else
            {

                CalculateReward();
                CalculateTarget();

                //Debug.Log($"Рассчитанно, цель {target} , награда {reward} CryptoCoin");

            }

        }
    }

    public override void SaveProgress()
    {
        if (GetTaskManager.GetSavedTasks != null)
        {
            Task st = GetTaskManager.GetSavedTasks.Find(t => t.NameTask == "Balance Task");
            st.progress = progress;
            st.target = target;
            st.reward = reward;
        }
    }

    public override void Deactivate()
    {
        SaveProgress();
        GetTaskManager.DeleteTask(this);
        Destroy(displayTaskItem.gameObject);
        GetTaskManager.ActiveUIOtherTasks();
        Destroy(this.gameObject);
    }

    public override void CalculateTarget()
    {
        if (progress == 0)
        {
            target = 500;
        }
        else
        {
            target *= 2;
        }
    }

    public override void CalculateReward()
    {
        if (reward == 0)
        {
            reward = 1;
        }
        else
        {
            reward ++;
        }
    }


    public override void Performance()
    {
        progress = (int)GetIncome.GetBalance*(int)GetCurentCurrency.GetRate - StartBalance;
        if ((progress + StartBalance) / GetCurentCurrency.GetRate >= target + StartBalance / GetCurentCurrency.GetRate)
        {
            //Debug.Log($"Задание выполенно награда {reward} CryptoCoin");
            StopCoroutine("CheckProgress");
            GetIncome.RewardCryptoCoin(reward);
            CalculateTarget();
            CalculateReward();
            progress = 0;
            Deactivate();
        }
        //else
        //{
        //    Debug.Log($"Стартовый баланс = {StartBalance/GetCurentCurrency.GetRate} Заработанно = {progress/GetCurentCurrency.GetRate} {GetCurentCurrency.GetNameCurrency}");
        //}


    }

    IEnumerator CheckProgress()
    {
        while (true)
        {
            Performance();
            yield return 0;
        }
    }

}
