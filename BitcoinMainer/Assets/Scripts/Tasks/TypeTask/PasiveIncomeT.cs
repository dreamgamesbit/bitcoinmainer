﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using static Manager.GameManager;
using UnityEngine;

public sealed class PasiveIncomeT : Task
{
    public override string NameTask { get; set; } = "Pasive Income Task";

    public override void Prime()
    {
        readyFA = true;
        LoadProgress();
        int finde = (from c in GetTaskManager.GetSavedTasks
                     where c is PasiveIncomeT
                     select c).Count();
        if (finde == 0)
        {
            GetTaskManager.GetSavedTasks.Add(new PasiveIncomeT());
        }
    }

    public override void Activate()
    {
        
        GetTaskManager.BlockUIOtherTasks(this);
        //Debug.Log($"Задание, разогнать пассивный доход до {target / GetCurentCurrency.GetRate } {GetCurentCurrency.GetNameCurrency}.");
        StartCoroutine("CheckProgress");
        
    }

    public override void LoadProgress()
    {
        if (GetTaskManager.GetSavedTasks != null)
        {
            IEnumerable<Task> st = from s in GetTaskManager.GetSavedTasks
                                   where s is PasiveIncomeT
                                   select s;

            if (st != null && st.Count() > 0)
            {
                foreach (Task item in st)
                {
                    if (item.target != 0)
                    {
                        progress = item.progress;
                        target = item.target;
                        reward = item.reward;
                    }
                    else
                    {

                        CalculateReward();
                        CalculateTarget();

                       // Debug.Log($"Рассчитанно, цель {target / GetCurentCurrency.GetRate} , награда {reward} {GetCurentCurrency.GetNameCurrency}");

                    }
                }
               // Debug.Log($"Прогресс загружен, пассивный доход {progress} , награда {reward} {GetCurentCurrency.GetNameCurrency}");
            }
            else
            {

                CalculateReward();
                CalculateTarget();

                //Debug.Log($"Рассчитанно, цель {target} , награда {reward} {GetCurentCurrency.GetNameCurrency}");

            }

        }
    }

    public override void SaveProgress()
    {
        if (GetTaskManager.GetSavedTasks != null)
        {
            Task st = GetTaskManager.GetSavedTasks.Find(t => t.NameTask == "Pasive Income Task");
            st.progress = progress;
            st.target = target;
            st.reward = reward;
        }
    }

    public override void Deactivate()
    {
        SaveProgress();
        GetTaskManager.DeleteTask(this);
        Destroy(displayTaskItem.gameObject);
        GetTaskManager.ActiveUIOtherTasks();
        Destroy(this.gameObject);
    }

    public override void CalculateTarget()
    {
        if (progress == 0)
        {
            target = 10;
        }
        else
        {
            target *= 5;
        }
    }

    public override void CalculateReward()
    {
        if (reward == 0)
        {
            reward = 1000;
        }
        else
        {
            reward = target * 10;
        }
    }

    public override void Performance()
    {
        progress = (int)GetCurentDevice.GetIncomePerSecondDevice;
        if (progress >= target )
        {
            //Debug.Log($"Задание выполенно награда {reward } {GetCurentCurrency.GetNameCurrency}");
            StopCoroutine("CheckProgress");            
            GetIncome.Reward(reward * GetCurentCurrency.GetRate);
            CalculateTarget();
            CalculateReward();
            Deactivate();
            progress = 0;
           
        }
        //else
        //{
        //    // Debug.Log($"Заработанно = {progress* GetCurentCurrency.GetRate} {GetCurentCurrency.GetNameCurrency}");
        //}


    }

    IEnumerator CheckProgress()
    {
        while (true)
        {
            Performance();
            yield return 0;
        }
    }
}
