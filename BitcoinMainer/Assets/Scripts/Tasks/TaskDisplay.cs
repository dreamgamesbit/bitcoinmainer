﻿
using System.Collections.Generic;
using UnityEngine.UI;
using static Manager.GameManager;
using UnityEngine;

public class TaskDisplay : MonoBehaviour
{
    Task prefTask;
    //ui
    #region Fields
    [Header("Main information")]
    [SerializeField]
    private protected Text DisplayNameTask;
    [SerializeField]
    private protected Text DisplayRewardTask;
    [SerializeField]
    private protected Text DisplayRewardTaskValue;
    [SerializeField]
    private protected Text DisplayTargetTaskName;
    [SerializeField]
    private protected Text DisplayTargetTaskValue;
    [SerializeField]
    private protected Text DisplayProgressTask;
    [SerializeField]
    private protected Button ButtonActivate;
    [SerializeField]
    private protected Image ButtonActivateImage;
    [SerializeField]
    private protected Sprite deactivateButtonSprite;
    [SerializeField]
    private protected Sprite activateButtonSprite;
    #endregion


    public Button GetButtonActivate
    {
        get => ButtonActivate;
    }

    public Text GetProgressTask
    {
        get => DisplayProgressTask;
    }

    public void DeactivateButton()
    {
        ButtonActivate.enabled = false;
        ButtonActivateImage.sprite = deactivateButtonSprite;
    }
    public void ActivateButton()
    {
        ButtonActivate.enabled = true;
        ButtonActivateImage.sprite = activateButtonSprite;
    }

    public void Prime(Task pref)
    {
        prefTask = pref;
        if (DisplayNameTask != null) DisplayNameTask.text = prefTask.NameTask;
        if (DisplayRewardTask != null) DisplayRewardTask.text = "Reward ";
        if (DisplayRewardTaskValue != null) DisplayRewardTaskValue.text = prefTask.reward.ToString() + " " + GetCurentCurrency.GetNameCurrency;
        if (DisplayTargetTaskName != null) DisplayTargetTaskName.text = "Clicks ";
        if (DisplayTargetTaskValue != null) DisplayTargetTaskValue.text = prefTask.target.ToString();
        if (DisplayProgressTask != null) DisplayProgressTask.text = "progress " + prefTask.progress.ToString();
    }

    public void TaskActivate()
    {
        prefTask.Activate();
    }
}
