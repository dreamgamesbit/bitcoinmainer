﻿using System.Linq;
using System;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using static Manager.GameManager;

public abstract class Task : MonoBehaviour
{
    
    internal int progress;
    internal int target;
    internal int reward;
    internal bool readyFA = false;
    internal TaskDisplay displayTaskItem;


    public abstract string NameTask
    {
        get; set;
    }
    public abstract void Prime();
 
    public abstract void Activate();

    public abstract void Performance();   

    public abstract void Deactivate();

    public abstract void CalculateTarget();

    public abstract void CalculateReward();

    public abstract void LoadProgress();


    public abstract void SaveProgress();
}
