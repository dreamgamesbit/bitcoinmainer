﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdvertManager : MonoBehaviour {

	[Tooltip("Period between ads. Set in seconds.")]
	public float showAdvertTimer;
    internal bool offAds;

	// Use this for initialization
	void Start () {
		showAdvertTimer = 150.0f;
	}
	
	// Update is called once per frame
	void Update () {
        if (!offAds)
        {
            showAdvertTimer -= Time.deltaTime;
		if (showAdvertTimer<=0){
			ShowAdvertisement();
            }
        }
        else { return; }
    }

	public void ShowAdvertisement(){
		if (Advertisement.IsReady()){
			Advertisement.Show();
			showAdvertTimer = 150.0f;
		}
	}
}
