﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


public class SaveManager : MonoBehaviour
{

      public static void SaveGameData(SaveData Data)
    {
        PlayerPrefs.SetString("log", JsonUtility.ToJson(Data));
    }


    public static SaveData LoadGameData()
    {
        if (PlayerPrefs.HasKey("log"))
        {            
            return JsonUtility.FromJson<SaveData>(PlayerPrefs.GetString("log"));
        }
        else return new SaveData(FindObjectOfType<Shop>().GetListAllDevices);
    }
}
    [System.Serializable]
    public class SaveData 
{
    //Device,Shop
    public string NameCurentDevice;
    public string NameCurentCurrency;
    public string [] AllDev;
    public string [] ItemAllDevice;
    public string [] CurrencysRateAllDevice;
    public string [] AllTasks;
    public string CurrentTask;
    public int [] dataChangeCurrencyRate;
    public int [] dataAddNewTask;
    public float Coin;
    public int DonatCoin;
    public bool tutorialMode;
    public bool modeAds;


    public int[] ClickTS;
    public int[] BalanceTS;
    public int[] PassiveIncTS;

    public SaveData(List<Device> ShopAllDevice)
    {
        NameCurentDevice = "Macintosh";
        NameCurentCurrency = "HCC";
        dataAddNewTask = null;
        dataChangeCurrencyRate = null;
        AllDev = new string[ShopAllDevice.Count];
        ItemAllDevice = new string[ShopAllDevice.Count];
        CurrencysRateAllDevice = new string[ShopAllDevice.Count];
        for (int i = 0; i < ShopAllDevice.Count; i++)
        {
            AllDev[i] += ShopAllDevice[i].GetIncomePerClickDevice.ToString();
            AllDev[i] += "|";
            AllDev[i] += ShopAllDevice[i].GetIncomePerSecondDevice.ToString();
            AllDev[i] += "|";
            AllDev[i] += ShopAllDevice[i].GetPurchased.ToString();
            for (int k = 0; k < ShopAllDevice[i].GetAvailablesUpgradeItems.Count; k++)
            {
                ItemAllDevice[i] += ShopAllDevice[i].GetAvailablesUpgradeItems[k].GetCoastItem.ToString();
                ItemAllDevice[i] += "*";
                ItemAllDevice[i] += ShopAllDevice[i].GetAvailablesUpgradeItems[k].GetDonatCoastItem.ToString();
                ItemAllDevice[i] += "*";
                ItemAllDevice[i] += ShopAllDevice[i].GetAvailablesUpgradeItems[k].GetClickForce.ToString();
                ItemAllDevice[i] += "*";
                ItemAllDevice[i] += ShopAllDevice[i].GetAvailablesUpgradeItems[k].GetSecondForce.ToString();
                ItemAllDevice[i] += "*";
                ItemAllDevice[i] += ShopAllDevice[i].GetAvailablesUpgradeItems[k].GetProcentUpgrade.ToString();
                ItemAllDevice[i] += "*";
                ItemAllDevice[i] += ShopAllDevice[i].GetAvailablesUpgradeItems[k].GetChanceDestroy.ToString();
                if (k!= ShopAllDevice[i].GetAvailablesUpgradeItems.Count-1)
                ItemAllDevice[i] += "|";
            }
            
            for (int k = 0; k < ShopAllDevice[i].GetAvailablesCurrency.Count; k++)
            {
                CurrencysRateAllDevice[i] += ShopAllDevice[i].GetAvailablesCurrency[k].GetRate.ToString();  
                if (k != ShopAllDevice[i].GetAvailablesCurrency.Count - 1)
                    CurrencysRateAllDevice[i] += "|";
            }
        }

        this.Coin = 0;
        this.DonatCoin = 0;
        tutorialMode = true;
        modeAds = false;



        //tasks
        AllTasks = null;
        CurrentTask = null;
        ClickTS = null;
        BalanceTS = null;
        PassiveIncTS = null;
    }

   
      
  
}


