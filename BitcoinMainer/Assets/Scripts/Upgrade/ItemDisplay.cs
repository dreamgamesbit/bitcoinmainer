﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Manager.GameManager;



public class ItemDisplay : MonoBehaviour {

    Item Item;
    #region Fields
    [Header("Main information")]
    [SerializeField]
    Image DisplayImageItem;
    [SerializeField]
    Text DisplayNameItem;
    [SerializeField]
    Text DisplayDescriptionItem;
    [SerializeField]
    Text DisplayCoastItem;
    [SerializeField]
    Text DisplayDonatCoastItem;
    [Header("Secondary information")]
    [SerializeField]
    Text DisplayClickForceItem;
    [SerializeField]
    Text DisplaySecondForceItem;
    [SerializeField]
    Image ProgressUpgrade;
    [SerializeField]
    Button PurchasItemButton;
    [SerializeField]
    Button DonatPurchasItemButton;
    #endregion

    #region Internals

    public Text GetDisplayCoastItem { get => DisplayCoastItem; set => DisplayCoastItem = value; }
    public Text GetDisplayClickForceItem { get => DisplayClickForceItem; set => DisplayClickForceItem = value; }
    public Text GetDisplaySecondForceItem { get => DisplaySecondForceItem; set => DisplaySecondForceItem = value; }
    public Image GetDisplayProgressUpgrade { get => ProgressUpgrade; set => ProgressUpgrade = value; }

    #endregion


    public void Prime (Item ItemPrefab)
    {
        Item = ItemPrefab;
        //Set items values to display items
        if (DisplayImageItem != null) DisplayImageItem.sprite = ItemPrefab.GetImageItem;
        if (DisplayNameItem != null) DisplayNameItem.text = ItemPrefab.GetNameItem;
        if (DisplayDescriptionItem != null) DisplayDescriptionItem.text = ItemPrefab.GetDescriptionItem;
        if (DisplayCoastItem != null) DisplayCoastItem.text = ItemPrefab.IndexCoinCounter();
        if (DisplayDonatCoastItem != null) DisplayDonatCoastItem.text = ItemPrefab.GetDonatCoastItem.ToString();
        if (ProgressUpgrade != null) ProgressUpgrade.fillAmount += ItemPrefab.GetProcentUpgrade / 100f;
        if (DisplayClickForceItem != null) DisplayClickForceItem.text = "Click +" + Math.Round(ItemPrefab.GetClickForce / GetCurentCurrency.GetRate, 2).ToString();
        if (DisplaySecondForceItem != null) DisplaySecondForceItem.text = "Second +" + Math.Round(ItemPrefab.GetSecondForce / GetCurentCurrency.GetRate, 2).ToString();

    }

    public void InfForPurchasItem()
    {
        FindObjectOfType<AudioManager>().Play("Click");
        GetUpgrade.PurchasItems(Item);
    }

    public void InfForDonatPurchasItem()
    {
        FindObjectOfType<AudioManager>().Play("Click");
        GetUpgrade.DonatPurchasItems(Item);
    }
}
