﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Manager.GameManager;

public class Item : MonoBehaviour {

    //Main information
    [SerializeField]
    Sprite ImageItem;
    [SerializeField]
    string NameItem;
    [Multiline,SerializeField]
    string DescriptionItem;

    [SerializeField]
    float CoastItem;
    [SerializeField]
    int DonatCoastItem;
    [SerializeField]
    float ClickForce;
    [SerializeField]
    float SecondForce;
    [SerializeField]
    int ProcentUpgrade;
    [SerializeField]
    int StepProcentUpgrade;
    [SerializeField]
    int ChanceDestroy = 0;

    #region Internals
    public Sprite GetImageItem { get => ImageItem; }

    public string GetNameItem { get => NameItem; }

    public string GetDescriptionItem { get => DescriptionItem; }

    public float GetCoastItem
    {
        get => CoastItem;

        set
        {
            if (value > 0) CoastItem = value;
        }
    }

    public int GetDonatCoastItem
    {
        get => DonatCoastItem;

        set
        {
            if (value > 0) DonatCoastItem = value;
        }
    }

    public float GetClickForce
    {
        get => ClickForce;

        set
        {
            if (value > 0) ClickForce = value;
        }
    }

    public float GetSecondForce
    {
        get => SecondForce;

        set
        {
            if (value > 0) SecondForce = value;
        }
    }

    public int GetProcentUpgrade
    {
        get => ProcentUpgrade;

        set
        {
            if (value >= 0) ProcentUpgrade = value;
        }
    }

    public int GetChanceDestroy
    {
        get => ChanceDestroy;
        set { if (value >= 0) ChanceDestroy = value; }
    }

    public ItemDisplay GetItemDisplay
    {
        get; set;
    }
    #endregion

    public bool UpgradeItemsCurentDevice(Device DeviceReference)
    {
       
        //Normal improvement before the possibility of breakage
        if (ProcentUpgrade <= 99)
        {
            //Increase force device
            DeviceReference.GetIncomePerClickDevice += ClickForce;
            DeviceReference.GetIncomePerSecondDevice += SecondForce;

            //Increase item values
            ProcentUpgrade += StepProcentUpgrade;
            ClickForce += GetCurentCurrency.GetRate;
            SecondForce += GetCurentCurrency.GetRate * 2;           
            CoastItem += ((GetCurentCurrency.GetRate * 2) + (GetCurentCurrency.GetRate * 4)) * 100.0f;           
            return false;
        }
        else
        {   //If a component breaks down
            if (ChanceDestroy >= UnityEngine.Random.Range(0, 100))
            {
                //Reduction force device                
                DeviceReference.GetIncomePerClickDevice -= ClickForce*4;
                DeviceReference.GetIncomePerSecondDevice -= SecondForce*4;

                //Reduction item values
                ProcentUpgrade -= StepProcentUpgrade * 4;
                ClickForce -= GetCurentCurrency.GetRate * 4;
                SecondForce -= GetCurentCurrency.GetRate * 4;                
                CoastItem += ((GetCurentCurrency.GetRate * 2) + (GetCurentCurrency.GetRate * 4)) * 100.0f;

                //Reset possible breakage
                ChanceDestroy = 0;
                return true;

            }
            //Improvement with the possibility of breakage
            else
            {
                //Increase force device
                DeviceReference.GetIncomePerClickDevice += ClickForce;
                DeviceReference.GetIncomePerSecondDevice += SecondForce;

                //Increase item values
                ProcentUpgrade += StepProcentUpgrade;
                ChanceDestroy += StepProcentUpgrade;
                ClickForce += GetCurentCurrency.GetRate * 2;
                SecondForce += GetCurentCurrency.GetRate * 2;
                CoastItem += ((GetCurentCurrency.GetRate * 1) + (GetCurentCurrency.GetRate * 1)) * 100.0f;           
                return false;
                
            }
        }
    }

    public void ConvertingDisplayValue()
    {
        if (GetItemDisplay.GetDisplayCoastItem != null) GetItemDisplay.GetDisplayCoastItem.text = IndexCoinCounter();
        if (GetItemDisplay.GetDisplayClickForceItem != null) GetItemDisplay.GetDisplayClickForceItem.text = "Click +" + Math.Round(ClickForce / GetCurentCurrency.GetRate, 2).ToString();
        if (GetItemDisplay.GetDisplaySecondForceItem != null) GetItemDisplay.GetDisplaySecondForceItem.text = "Second +" + Math.Round(SecondForce / GetCurentCurrency.GetRate, 2).ToString();
    }

    public string IndexCoinCounter()
    {
        if (CoastItem / GetCurentCurrency.GetRate >= 1000) return Math.Round((CoastItem / GetCurentCurrency.GetRate) / 1000, 1).ToString() + 'k' + " " + GetCurentCurrency.GetNameCurrency;
        else return Math.Round(CoastItem / GetCurentCurrency.GetRate).ToString() + " " + GetCurentCurrency.GetNameCurrency;
    }

    public bool DonatUpgradeItemsCurentDevice(Device DeviceReference)
    {
        //Donat Increase force device
        DeviceReference.GetIncomePerClickDevice += ClickForce;
        DeviceReference.GetIncomePerSecondDevice += SecondForce;
        //Donat Increase item values
        ClickForce += GetCurentCurrency.GetRate;
        SecondForce += GetCurentCurrency.GetRate*2;       
        return false;
    }


}
