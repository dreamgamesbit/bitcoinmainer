﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Manager.GameManager;

public class Upgrade : MonoBehaviour {

    [SerializeField]
    UpgrdeDisplay UpgrdeDisplay;
    #region Internals
    public UpgrdeDisplay GetDisplayUpgrade { get; private set; }
    #endregion
    
    void Start()
    {
        //Create Upgrade window
        GetDisplayUpgrade = Instantiate(UpgrdeDisplay);

        //Inizialize items current device in upgrade window
        GetDisplayUpgrade.PrimeItems(GetCurentDevice.GetAvailablesUpgradeItems);

        //Deactivate Upgrade Window
        GetDisplayUpgrade.gameObject.SetActive(false);
    }

    public void OpenUpgrade()
    {
        //Activate Upgrade Window
        GetDisplayUpgrade.gameObject.SetActive(true);
    }

    public void SetForCurentDeviceUpgrade()
    {
        //Destroy old upgrade window, in order to display upgrade items of the new Devices
        Destroy(GetDisplayUpgrade.gameObject);

        //Re-creation upgrade window
        GetDisplayUpgrade = Instantiate(UpgrdeDisplay);

        //Re-Inizialize items current device in upgrade window
        GetDisplayUpgrade.PrimeItems(GetCurentDevice.GetAvailablesUpgradeItems);

        //Deactivate Upgrade Window
        GetDisplayUpgrade.gameObject.SetActive(false);

    }

    public void PurchasItems(Item ItemRefernce)
    {
        //Occurs if current balance greater then coast item 
        if ((float)GetIncome.GetBalance >= ItemRefernce.GetCoastItem / GetCurentCurrency.GetRate)
        {
            //Stop mining currency
            GetIncome.StopCoroutine("CoinPerSecond");

            //Subtract the cost item from the balance
            GetIncome.GetBalance -= (decimal)(ItemRefernce.GetCoastItem / GetCurentCurrency.GetRate);

            //Run mining currency
            GetIncome.StartCoroutine("CoinPerSecond");

            //Change item values
            ItemRefernce.UpgradeItemsCurentDevice(GetCurentDevice);

            //Smoth filling improvement scale
            StartCoroutine(SmothProgressUpgradeItems(ItemRefernce.GetProcentUpgrade / 100f, ItemRefernce.GetItemDisplay, ItemRefernce.GetItemDisplay.GetDisplayProgressUpgrade.fillAmount));

            //Change display item values
            ItemRefernce.GetItemDisplay.GetDisplayCoastItem.text = Math.Round(ItemRefernce.GetCoastItem / GetCurentCurrency.GetRate).ToString() + " " + GetCurentCurrency.GetNameCurrency;
            ItemRefernce.GetItemDisplay.GetDisplayClickForceItem.text = "Click +" + Math.Round(ItemRefernce.GetClickForce / GetCurentCurrency.GetRate, 2).ToString();
            ItemRefernce.GetItemDisplay.GetDisplaySecondForceItem.text = "Second +" + Math.Round(ItemRefernce.GetSecondForce / GetCurentCurrency.GetRate, 2).ToString();

            //Update display values (force) Device in Shop device 
            GetCurentDevice.GetDeviceDisplay.GetDisplayIncomePerClickDevice.text = "Per Click\n" + Math.Round(GetCurentDevice.GetIncomePerClickDevice / GetCurentCurrency.GetRate).ToString();
            GetCurentDevice.GetDeviceDisplay.GetDisplayIncomePerSecondDevice.text = "Per Second\n" + Math.Round(GetCurentDevice.GetIncomePerSecondDevice / GetCurentCurrency.GetRate).ToString();
        }
    }

    public void DonatPurchasItems(Item ItemRefernce)
    {
        if ((float)GetIncome.GetDonatBalance >= ItemRefernce.GetDonatCoastItem)
        {
            //Stop mining currency
            GetIncome.StopCoroutine("CoinPerSecond");

            //Subtract the cost item from the donat balance
            GetIncome.GetDonatBalance -= ItemRefernce.GetDonatCoastItem;

            //Run mining currency
            GetIncome.DisplayDonatCoin();

            //Run mining currency
            GetIncome.StartCoroutine("CoinPerSecond");

            //Change item values
            ItemRefernce.DonatUpgradeItemsCurentDevice(GetCurentDevice);

            //Change display item values
            ItemRefernce.GetItemDisplay.GetDisplayClickForceItem.text = "Click +" + Math.Round(ItemRefernce.GetClickForce / GetCurentCurrency.GetRate, 2).ToString();
            ItemRefernce.GetItemDisplay.GetDisplaySecondForceItem.text = "Second +" + Math.Round(ItemRefernce.GetSecondForce / GetCurentCurrency.GetRate, 2).ToString();

            //Update display values (force) Device in Shop device 
            GetCurentDevice.GetDeviceDisplay.GetDisplayIncomePerClickDevice.text = "Per Click\n" + Math.Round(GetCurentDevice.GetIncomePerClickDevice / GetCurentCurrency.GetRate).ToString();
            GetCurentDevice.GetDeviceDisplay.GetDisplayIncomePerSecondDevice.text = "Per Second\n" + Math.Round(GetCurentDevice.GetIncomePerSecondDevice / GetCurentCurrency.GetRate).ToString();

        }
    }

    IEnumerator SmothProgressUpgradeItems(float Progress, ItemDisplay ItemDisplayRef, float minimum)
    {
        float t = 0, min = minimum, max = Progress;
        while (t<1.1f)
        {
            ItemDisplayRef.GetDisplayProgressUpgrade.fillAmount = Mathf.Lerp(min, max, t);
            t += 0.5f * Time.deltaTime; //if speed=2 then time has been slowly on two points             

            yield return 0;
        }
        
    }
}
