﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Manager.GameManager;

public class UpgrdeDisplay : MonoBehaviour {

    [SerializeField]
    GameObject TargetTransformItems;
    [SerializeField]
    ItemDisplay ItemDisplayPrefab;
    [SerializeField]
    Image BackGround;
    List<ItemDisplay> ItemInit = new List<ItemDisplay>();

    
    void Start()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;

        //Set background upgrade window
        BackGround.sprite = GetCurentDevice.GetWallPaper;

    }

    public void PrimeItems(List<Item> ItemList)
    {
        //Inizialize item upgrade objects
        for (int i = 0; i < ItemList.Count; i++)
        {
            ItemInit.Add(new ItemDisplay());
            ItemInit[i] =Instantiate(ItemDisplayPrefab);
            ItemInit[i].transform.SetParent(TargetTransformItems.transform, false);
            //Inizialize display item
            ItemInit[i].Prime(ItemList[i]);
            ItemList[i].GetItemDisplay = ItemInit[i];
        }

       
    }
    
    public void CloseUpgrade()
    {
        FindObjectOfType<AudioManager>().Play("Click");
        GetMenu.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}
