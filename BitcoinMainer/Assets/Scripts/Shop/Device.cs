﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Manager.GameManager;

public class Device : MonoBehaviour
{


    [SerializeField]
    Sprite ImageDevice;
    [SerializeField]
    string NameDevice;
    [Multiline]
    [SerializeField]
    string DescriptionDevice;
    [SerializeField]
    float Coast;
    [SerializeField]
    int DonatCoast;

    [SerializeField]
    float IncomePerClickDevice;
    [SerializeField]
    float IncomePerSecondDevice;
    [SerializeField]
    List<Item> AvailablesUpgradeItems;
    [SerializeField]
    List<Currency> AvailablesCurrency;
    [SerializeField]
    bool Purchased;

    [SerializeField]
    Sprite RoomSprite;
    [SerializeField]
    Sprite WallPaper;
    [SerializeField]
    Sprite IconMenu;



    #region Internals
    public Sprite GetImageDevice
    {
        get => ImageDevice; private set => ImageDevice = value;
    }

    public string GetNameDevice
    {
        get => NameDevice; private set => NameDevice = value;
    }

    public string GetDescriptionDevice
    {
        get => DescriptionDevice; private set => DescriptionDevice = value;
    }

    public float GetCoast
    {
        get => Coast; private set => Coast = value;
    }

    public int GetDonatCoast
    {
        get => DonatCoast; private set => DonatCoast = value;
    }

    public float GetIncomePerClickDevice
    {
        get => IncomePerClickDevice; set { if ( value >= 0 ) IncomePerClickDevice = value; }
    }

    public float GetIncomePerSecondDevice
    {
        get => IncomePerSecondDevice; set { if (value >= 0) IncomePerSecondDevice = value; }
    }

    public List<Item> GetAvailablesUpgradeItems
    {
        get => AvailablesUpgradeItems; private set => AvailablesUpgradeItems = value;
    }

    public List<Currency> GetAvailablesCurrency
    {
        get => AvailablesCurrency; private set => AvailablesCurrency = value;
    }
    
    public DeviceDisplay GetDeviceDisplay { get; set; }

    public bool GetPurchased { get=>Purchased; set=>Purchased=value; }

    public Sprite GetRoomSprite
    {
        get => RoomSprite; private set => RoomSprite = value;
    }

    public Sprite GetWallPaper
    {
        get => WallPaper; private set => WallPaper = value;
    }

    public Sprite GetIconMenu
    {
        get => IconMenu; private set => IconMenu = value;
    }

    #endregion

    public void ConvertingDisplayValue()
    {
        //Inizialize device display
        if (GetDeviceDisplay.GetDisplayCoastDevice != null) GetDeviceDisplay.GetDisplayCoastDevice.text = Purchased ? "Switch" : IndexCoinCounter();
        if (GetDeviceDisplay.GetDisplayIncomePerClickDevice != null) GetDeviceDisplay.GetDisplayIncomePerClickDevice.text = "Per Click\n" + Math.Round(IncomePerClickDevice / GetCurentCurrency.GetRate, 2).ToString();
        if (GetDeviceDisplay.GetDisplayIncomePerSecondDevice != null) GetDeviceDisplay.GetDisplayIncomePerSecondDevice.text = "Per Second\n" + Math.Round(IncomePerSecondDevice / GetCurentCurrency.GetRate, 2).ToString();
    }

    public string IndexCoinCounter()
    {
        //Add index to display coats device
        if (Coast / GetCurentCurrency.GetRate >= 1000 && Coast / GetCurentCurrency.GetRate <1000000) return Math.Round((Coast / GetCurentCurrency.GetRate) / 1000, 1).ToString() + 'K' + " " + GetCurentCurrency.GetNameCurrency;
        else if (Coast / GetCurentCurrency.GetRate >= 1000000 && Coast / GetCurentCurrency.GetRate < 1000000000) return Math.Round((Coast / GetCurentCurrency.GetRate) / 1000000, 1).ToString() + 'M' + " " + GetCurentCurrency.GetNameCurrency; 
        else return Math.Round(Coast / GetCurentCurrency.GetRate).ToString() + " " + GetCurentCurrency.GetNameCurrency;
    }
    
}
   
