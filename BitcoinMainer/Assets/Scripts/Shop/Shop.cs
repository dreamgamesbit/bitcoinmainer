﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Manager.GameManager;

public class Shop : MonoBehaviour {
    [SerializeField]
    List<Device> ListAllDevices = new List<Device>();
    [SerializeField]
    ShopDisplay ShopDisplayPrefab;    
    #region Internals
    public List<Device> GetListAllDevices { get => ListAllDevices; private set => ListAllDevices = value; }
    public ShopDisplay GetShopDisplay { get; private set; }
    #endregion

    void Start ()
    {
        //Initialize displlay shop and display all device
        GetShopDisplay = Instantiate(ShopDisplayPrefab);
        GetShopDisplay.Prime(ListAllDevices);

        //Load saved current device, after initialaze all device 
        GetGameManager.LoadCurentDevice();

        //Deactivate Shop window
        GetShopDisplay.gameObject.SetActive(false);
    }

    public void OpenShop()
    {
        //Avtivate Shop window
        if (GetShopDisplay != null) GetShopDisplay.gameObject.SetActive(true);       
    }


    public void PurchasDevice(Device DeviceRefernce)
    {
        //If Device not bathing purchase device purchase occurs, else there is a change of devices
        if (DeviceRefernce.GetPurchased == false)
        {
            if ((float)GetIncome.GetBalance >= DeviceRefernce.GetCoast / GetCurentCurrency.GetRate)
            {
                //Deactivate donat purchas button, offset purchase button and change button function from purchase to switch
                DeviceRefernce.GetDeviceDisplay.GetDonatPurchasDeviceButton.gameObject.SetActive(false);
                DeviceRefernce.GetDeviceDisplay.GetPurchasDeviceButton.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, DeviceRefernce.GetDeviceDisplay.GetPurchasDeviceButton.GetComponent<RectTransform>().anchoredPosition.y);
                DeviceRefernce.GetDeviceDisplay.GetDisplayCoastDevice.text = "Switch";

                //Stop maining, deducting the balance of the cost of the device, run maining
                GetIncome.StopCoroutine("CoinPerSecond");
                GetIncome.GetBalance -= (decimal)(DeviceRefernce.GetCoast / GetCurentCurrency.GetRate);
                GetIncome.StartCoroutine("CoinPerSecond");

                //Device puchaased
                DeviceRefernce.GetPurchased = true;

                //Make this device current 
                GetGameManager.SetCurentNewDevice(DeviceRefernce);
                
            }

        }
        else
        {
            //Make this device current 
            GetGameManager.SetCurentNewDevice(DeviceRefernce);
        }
    }

    public void DonatPurchasDevice(Device DeviceRefernce)
    {
        if (DeviceRefernce.GetPurchased == false)
        {
            if (GetIncome.GetDonatBalance >= DeviceRefernce.GetDonatCoast )

            {
                //Deactivate donat purchas button, offset purchase button and change button function from purchase to switch
                DeviceRefernce.GetDeviceDisplay.GetDonatPurchasDeviceButton.gameObject.SetActive(false);
                DeviceRefernce.GetDeviceDisplay.GetPurchasDeviceButton.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, DeviceRefernce.GetDeviceDisplay.GetPurchasDeviceButton.GetComponent<RectTransform>().anchoredPosition.y);
                DeviceRefernce.GetDeviceDisplay.GetDisplayCoastDevice.text = "Switch";

                //Deducting the donat balance of the cost of the device
                GetIncome.GetDonatBalance -= DeviceRefernce.GetDonatCoast;

                //Re-display donat coin
                GetIncome.DisplayDonatCoin();

                //Device puchaased
                DeviceRefernce.GetPurchased = true;

                //Make this device current 
                GetGameManager.SetCurentNewDevice(DeviceRefernce);

            }

        }
        
    }



}
