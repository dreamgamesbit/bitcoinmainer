﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using static Manager.GameManager;

public class ShopDisplay : MonoBehaviour {

    [SerializeField]
    Transform TargetTransform;
    [SerializeField]
    DeviceDisplay DeviceDisplayPrefab;
    [SerializeField]
    SnapScrolling targetScroll;
    [SerializeField]
    Image BackGround;

    List<DeviceDisplay> DeviceDisplayInit = new List<DeviceDisplay>();

    public Image GetBackGround { get => BackGround; private set => BackGround = value; }

 
    void Start () {

        GetComponent<Canvas>().worldCamera = Camera.main;
        BackGround.sprite = GetCurentDevice.GetWallPaper;
    }
	
    public void Prime (List<Device> DeviceList)
    {

        for (int i = 0; i < DeviceList.Count; i++)
        {
            DeviceDisplayInit.Add(new DeviceDisplay());
            DeviceDisplayInit[i] = Instantiate(DeviceDisplayPrefab);
            DeviceDisplayInit[i].transform.SetParent(TargetTransform, false);
            DeviceDisplayInit[i].Prime(DeviceList[i]);
            DeviceList[i].GetDeviceDisplay = DeviceDisplayInit[i];
        }
        
        targetScroll.InitializedDeviceDisplay(DeviceDisplayInit);       
    }


    public void CloseShop()
    {
        FindObjectOfType<AudioManager>().Play("Click");
        GetMenu.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}
