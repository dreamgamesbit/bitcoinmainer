﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Manager.GameManager;

public class DeviceDisplay : MonoBehaviour {

    Device Devise;

    [Header("Main information")]
    [SerializeField]
    Image DisplayImageDevice;
    [SerializeField]
    Text DisplayNameDevice;
    [SerializeField]
    Text DisplayDescriptionDevice;
    [SerializeField]
    Text DisplayCoastDevice;
    [SerializeField]
    Text DisplayDonatCoastDevice;

    [Header("Secondary information")]
    [SerializeField]
    Text DisplayIncomePerClickDevice;
    [SerializeField]
    Text DisplayIncomePerSecondDevice;
    [SerializeField]
    Button PurchasDeviceButton;
    [SerializeField]
    Button DonatPurchasDeviceButton;

    #region Internals
    public Text GetDisplayCoastDevice { get => DisplayCoastDevice; set => DisplayCoastDevice = value; }
    public Text GetDisplayIncomePerClickDevice { get => DisplayIncomePerClickDevice; set => DisplayIncomePerClickDevice = value; }
    public Text GetDisplayIncomePerSecondDevice { get => DisplayIncomePerSecondDevice; set => DisplayIncomePerSecondDevice = value; }
    public Button GetPurchasDeviceButton { get => PurchasDeviceButton; private set => PurchasDeviceButton = value; }
    public Button GetDonatPurchasDeviceButton { get => DonatPurchasDeviceButton; private set => DonatPurchasDeviceButton = value; }
    #endregion


    public void Prime (Device DevicePrefab)    {

        Devise = DevicePrefab;

        //Inizialize display device elements
        if (DisplayImageDevice != null) DisplayImageDevice.sprite = DevicePrefab.GetImageDevice;
        if (DisplayNameDevice != null) DisplayNameDevice.text = DevicePrefab.GetNameDevice;
        if (DisplayDescriptionDevice != null) DisplayDescriptionDevice.text = DevicePrefab.GetDescriptionDevice;              
        if (DisplayIncomePerClickDevice != null) DisplayIncomePerClickDevice.text = "Per Click\n" + Math.Round(DevicePrefab.GetIncomePerClickDevice / GetCurentCurrency.GetRate,2).ToString();
        if (DisplayIncomePerSecondDevice != null) DisplayIncomePerSecondDevice.text = "Per Second\n" + Math.Round(DevicePrefab.GetIncomePerSecondDevice / GetCurentCurrency.GetRate,2).ToString();
        if (PurchasDeviceButton != null)
        {

            PurchasDeviceButton.GetComponent<Button>();

            //If Device not bathing display the coast on the Purchas button, else display the shift Purchas button device
            if (DevicePrefab.GetPurchased == false)
            {
                if (DisplayCoastDevice != null)
                {
                    DisplayCoastDevice.text = DevicePrefab.IndexCoinCounter();
                }
            }
            else
            {
                DisplayCoastDevice.text = "Switch";
                PurchasDeviceButton.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, PurchasDeviceButton.GetComponent<RectTransform>().anchoredPosition.y);
            }
            PurchasDeviceButton.onClick.AddListener(InfForPurchasDevice);
        }

        //If Device not bathing display the donat coast on the donat Purchas button, else diactivate donat Purchas button device
        if (DonatPurchasDeviceButton != null)
        {

            DonatPurchasDeviceButton.GetComponent<Button>();

            if (DevicePrefab.GetPurchased == false)
            {
                if (DisplayDonatCoastDevice != null)
                {
                    DisplayDonatCoastDevice.text = DevicePrefab.GetDonatCoast.ToString();
                    DonatPurchasDeviceButton.onClick.AddListener(InfForDonatPurchasDevice);
                }
            }
            else
            {
                DonatPurchasDeviceButton.gameObject.SetActive(false);
            }
            
        }

    }

    public void InfForPurchasDevice()
    {       
        FindObjectOfType<AudioManager>().Play("Click");
        GetShop.PurchasDevice(Devise);
    }

    public void InfForDonatPurchasDevice()
    {
        
        FindObjectOfType<AudioManager>().Play("Click");
        GetShop.DonatPurchasDevice(Devise);
    }
}
