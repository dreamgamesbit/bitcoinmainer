﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 public class SnapScrolling : MonoBehaviour {


    List<DeviceDisplay> DeviceLisDisplay;
    Vector2 [] PositionDeviceDisplay;
    [SerializeField]
    Vector3 SizeBoxDisplayDevice;

    float NearestPositionDevice, spacing;
    
    int IDNearestDevice = 0;
    bool scrollon;

    Vector2 setNearesDevicePos;
    float speedSetPosDevice = 10f;

    public void InitializedDeviceDisplay(List<DeviceDisplay> LisDevice)
    {
        DeviceLisDisplay = new List<DeviceDisplay> (LisDevice.Count);
        DeviceLisDisplay = LisDevice;
        PositionDeviceDisplay = new Vector2 [DeviceLisDisplay.Count];

    }

    public float CalculatePosition()
    {                     
       for (int i = 1; i < DeviceLisDisplay.Count; i++)
        {
            PositionDeviceDisplay[i] = -DeviceLisDisplay[i].transform.localPosition + SizeBoxDisplayDevice;              
        }

       return PositionDeviceDisplay[DeviceLisDisplay.Count-1].x;

    }

    private void FixedUpdate()
    {
        int ID = CalculatedNearestDevice();

        if (!scrollon) 
        {
            setNearesDevicePos.x = Mathf.SmoothStep(this.GetComponent<RectTransform>().anchoredPosition.x, PositionDeviceDisplay[ID].x, speedSetPosDevice * Time.fixedDeltaTime);
            GetComponent<RectTransform>().anchoredPosition = setNearesDevicePos;
        }

    }

    int CalculatedNearestDevice()
    {
        NearestPositionDevice = float.MaxValue;
        
        for (int i = 0; i < DeviceLisDisplay.Count; i++)
        {
            spacing = Mathf.Abs( this.GetComponent<RectTransform>().anchoredPosition.x - PositionDeviceDisplay[i].x);
            
            if (spacing < NearestPositionDevice)
            {                
                NearestPositionDevice = spacing;
                IDNearestDevice = i; 
                
            }
        }

        return IDNearestDevice;
    }

    public void EnebledScroll(bool scrollon)
    {
        this.scrollon = scrollon;
    }
}
