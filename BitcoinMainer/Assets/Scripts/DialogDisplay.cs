﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogDisplay : MonoBehaviour {

    public Text DescriptionArea;


    public void Prime(string Description)
    {
        DescriptionArea.text = Description;
    }

    public void CloseWindow()
    {
        Destroy(gameObject);
    }


}
