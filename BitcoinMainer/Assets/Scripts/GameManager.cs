﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



namespace Manager
{

    public class GameManager : MonoBehaviour
    {
        #region private fields
        [SerializeField]
        SpriteRenderer RoomSpriteCurentDevice;
        [SerializeField]
        Menu MenuPrefab;
        //Image MenuBackGround;
        SaveData Save;             
        //Dictionary<string, float> CurrencyValue;
        [SerializeField]
        float Timeleft;
        [SerializeField]
        int RangeChangeCurrencyRate;


        TimeSpan timeChangeInterval, nextTimeChange;
        #endregion

        #region internals

        static public GameManager GetGameManager
        {
            get;
            private set;
        }

        //module internals 

        static public Graph GetGraph
        {
            get; private set;
        }

        static public Shop GetShop
        {
            get; private set;
        }

        static public Income GetIncome
        {
            get; private set;
        }

        static public Upgrade GetUpgrade
        {
            get; private set;
        }

        static public AdvertManager GetAdvertManager
        {
            get; private set;
        }

        static public TaskManger GetTaskManager
        {
            get; private set;
        }
        
        //fields internals
        static public Currency GetCurentCurrency
        {
            get; private set;
        }

        static public Device GetCurentDevice
        {
            get; private set;
        }

        static public Menu GetMenu
        {
            get; private set;
        }
        
        public float GetTimeLeft
        {
            get => Timeleft; 
        }

        public int GetRangeChangeCurrencyRate
        {
            get => RangeChangeCurrencyRate;
        }

        public SaveData GetSave
        {
            get => Save;
        }

        #endregion

        //Temporarily
        private void Awake()
        {
            //inizialize internals
            GetGameManager = this;
            GetShop = GetComponent<Shop>();
            GetIncome = GetComponent<Income>();
            GetUpgrade = GetComponent<Upgrade>();
            GetGraph = GetComponent<Graph>();
            GetTaskManager = GetComponent<TaskManger>();
            GetAdvertManager = GetComponent<AdvertManager>();            
           

            LoadGame();
        }

        // Use this for initialization
        void Start()
        {

            //Instantiate PC prefab,available currency and available upgrade items
            InicialazingPC(GetShop.GetListAllDevices);


            //Set values from all upgrade items of save file
            for (int i = 0; i < GetShop.GetListAllDevices.Count; i++)
            {
                string[] splitListDeviceParam = Save.AllDev[i].Split('|');
                GetShop.GetListAllDevices[i].GetIncomePerClickDevice = float.Parse(splitListDeviceParam[0]);
                GetShop.GetListAllDevices[i].GetIncomePerSecondDevice = float.Parse(splitListDeviceParam[1]);
                GetShop.GetListAllDevices[i].GetPurchased = Convert.ToBoolean(splitListDeviceParam[2]);

                string[] splitListDeviceItem = Save.ItemAllDevice[i].Split('|');
                for (int k = 0; k < splitListDeviceItem.Length; k++)
                {
                    string[] splitListDeviceItemParam = splitListDeviceItem[k].Split('*');
                    GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetCoastItem = float.Parse(splitListDeviceItemParam[0]);
                    GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetDonatCoastItem = int.Parse(splitListDeviceItemParam[1]);
                    GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetClickForce = float.Parse(splitListDeviceItemParam[2]);
                    GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetSecondForce = float.Parse(splitListDeviceItemParam[3]);
                    GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetProcentUpgrade = int.Parse(splitListDeviceItemParam[4]);
                    GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetChanceDestroy = int.Parse(splitListDeviceItemParam[5]);
                }

                string[] splitListDeviceCurrency = Save.CurrencysRateAllDevice[i].Split('|');
                for (int k = 0; k < splitListDeviceCurrency.Length; k++)
                {
                    GetShop.GetListAllDevices[i].GetAvailablesCurrency[k].GetRate = float.Parse(splitListDeviceCurrency[k]);
                }
            }


            timeChangeInterval = new TimeSpan(0, 60, 00);

            if (Save.dataChangeCurrencyRate == null)
            {
                nextTimeChange = timeChangeInterval + DateTime.Now.TimeOfDay;

                Save.dataChangeCurrencyRate = new int[]
                {
                    DateTime.Now.Year,
                    DateTime.Now.Month,
                    DateTime.Now.Day,
                    nextTimeChange.Hours,
                    nextTimeChange.Minutes,
                    nextTimeChange.Seconds,
                    nextTimeChange.Milliseconds,

                };
            }
            else
            {
                DateTime savedDataChangeCurrencyRate = new DateTime(

                    Save.dataChangeCurrencyRate[0], 
                    Save.dataChangeCurrencyRate[1], 
                    Save.dataChangeCurrencyRate[2], 
                    Save.dataChangeCurrencyRate[3],
                    Save.dataChangeCurrencyRate[4],
                    Save.dataChangeCurrencyRate[5],
                    Save.dataChangeCurrencyRate[6]
                
                    );

                TimeSpan checkdate = savedDataChangeCurrencyRate - DateTime.Now;

                if (checkdate.TotalSeconds > 0)
                {
                    nextTimeChange = savedDataChangeCurrencyRate.TimeOfDay;
                }
                else
                {
                    nextTimeChange = timeChangeInterval + DateTime.Now.TimeOfDay;

                    Save.dataChangeCurrencyRate = new int[]
                    {
                        DateTime.Now.Year,
                        DateTime.Now.Month,
                        DateTime.Now.Day,
                        nextTimeChange.Hours,
                        nextTimeChange.Minutes,
                        nextTimeChange.Seconds,
                        nextTimeChange.Milliseconds,

                    };
                    ChangeRateCurrency(GetShop.GetListAllDevices);

                }
                
            }
                        
            Timeleft =(float)(nextTimeChange - DateTime.Now.TimeOfDay).TotalSeconds;

            StartCoroutine("TimeChangeCurrenceRate");

            //Set Curent device of save file
            GetCurentDevice = GetShop.GetListAllDevices.Find(x => x.GetNameDevice == Save.NameCurentDevice);

            //Set Curent currence of save file
            GetCurentCurrency = GetCurentDevice.GetAvailablesCurrency.Find(x => x.GetNameCurrency == Save.NameCurentCurrency);

            //advertisement
            GetAdvertManager.offAds = Save.modeAds;
                     
            //Create menu
            GetMenu = Instantiate(MenuPrefab);
        }

        private void InicialazingPC(List<Device> ListAllDevice)
        {
          

            for (int i = 0; i < ListAllDevice.Count; i++)
            {
                //Create all device in scene
                ListAllDevice[i] = Instantiate(ListAllDevice[i], this.gameObject.transform);

                //Target object in scene for availables currency on every device
                GameObject TargetCurrency = new GameObject("TargetCurrency");
                TargetCurrency.transform.SetParent(ListAllDevice[i].gameObject.transform);

                //Create availables currency object in scene
                for (int k = 0; k < ListAllDevice[i].GetAvailablesCurrency.Count; k++)
                {
                    ListAllDevice[i].GetAvailablesCurrency[k] = Instantiate(ListAllDevice[i].GetAvailablesCurrency[k]);
                    ListAllDevice[i].GetAvailablesCurrency[k].CalculatemaxRateRange();
                    ListAllDevice[i].GetAvailablesCurrency[k].transform.SetParent(TargetCurrency.transform);
                }

                //Target object in scene for availables upgrade items on every device
                GameObject TargetItems = new GameObject("TargetItems");
                TargetItems.transform.SetParent(ListAllDevice[i].gameObject.transform);

                //Create availables upgrade items object in scene 
                for (int k = 0; k < ListAllDevice[i].GetAvailablesUpgradeItems.Count; k++)
                {
                    ListAllDevice[i].GetAvailablesUpgradeItems[k] = Instantiate(ListAllDevice[i].GetAvailablesUpgradeItems[k]);
                    ListAllDevice[i].GetAvailablesUpgradeItems[k].transform.SetParent(TargetItems.transform);
                }

                //Deactivate all device
                ListAllDevice[i].gameObject.SetActive(false);
            }

        }

        public void LoadCurentDevice()
        {
            //Disabled purchas button on current device
            GetCurentDevice.GetDeviceDisplay.GetPurchasDeviceButton.gameObject.SetActive(false);

            //Set curent device image in room
            RoomSpriteCurentDevice.sprite = GetCurentDevice.GetRoomSprite;   

            //Activate Current device
            GetCurentDevice.gameObject.SetActive(true);
        }

        public void SetCurentNewDevice(Device DeviceRefrence)
        {
            //Activate swtch/buy button
            GetCurentDevice.GetDeviceDisplay.GetPurchasDeviceButton.gameObject.SetActive(true);
            GetCurentDevice.GetDeviceDisplay.GetDisplayCoastDevice.text = "Switch";
            GetCurentDevice.GetDeviceDisplay.GetPurchasDeviceButton.enabled = true;

            //Set new device         
            GetCurentDevice.gameObject.SetActive(false);
            GetCurentDevice = DeviceRefrence;
            GetCurentDevice.gameObject.SetActive(true);

            //Switch BackGround monitor (menu) 
            SwitchBackGround();

            //Switch upgrade items for device
            GetUpgrade.SetForCurentDeviceUpgrade();

            //Set first available currency in device
            SetCurentCurrency(GetCurentDevice.GetAvailablesCurrency[0]);   
            
            //Switch image device in room
            RoomSpriteCurentDevice.sprite = GetCurentDevice.GetRoomSprite;

            //Disabled button purchased
            GetCurentDevice.GetDeviceDisplay.GetPurchasDeviceButton.gameObject.SetActive(false);
            GetCurentDevice.GetDeviceDisplay.GetPurchasDeviceButton.enabled = false;

            //Switch available currency in graph
            GetGraph.Switchdevice = true;
        }

        void SwitchBackGround()
        {
            //Switch background all windows
            GetShop.GetShopDisplay.GetBackGround.sprite = GetCurentDevice.GetWallPaper;
            GetGraph.GetGraphDisplay.GetBackGround.sprite = GetCurentDevice.GetWallPaper;
            GetMenu.GetBackGround.sprite = GetCurentDevice.GetWallPaper;
        }

        public void SetCurentCurrency(Currency NewCurrency)
        {            
            //Converting curent balance
            GetIncome.StopCoroutine("CoinPerSecond");
            float CurentCoinBalance = (float)GetIncome.GetBalance * GetCurentCurrency.GetRate;
            CurentCoinBalance = CurentCoinBalance / NewCurrency.GetRate;
            GetIncome.GetBalance = (decimal)CurentCoinBalance;
            GetIncome.StartCoroutine("CoinPerSecond");

            //Set New Curent Currency
            GetCurentCurrency = NewCurrency;

            //Converting value Device (Coast and force)
            foreach (var Device in GetShop.GetListAllDevices)
            {
                Device.ConvertingDisplayValue();
            }

            //Converting value upgrade item curent device (Coast and force)
            foreach (var Item in GetCurentDevice.GetAvailablesUpgradeItems)
            {
                Item.ConvertingDisplayValue();
            }
        }

        public void ChangeRateCurrency(List<Device> ListAllDevice)
        {
            
            System.Random rRange = new System.Random();
            System.Random rChance = new System.Random();
            for (int dev = 0; dev < ListAllDevice.Count; dev++)
            {
                
                for (int cur = 0; cur < ListAllDevice[dev].GetAvailablesCurrency.Count; cur++)
                {
                    string debug = ListAllDevice[dev].GetAvailablesCurrency[cur].GetRate.ToString();
                    ListAllDevice[dev].GetAvailablesCurrency[cur].CalculateNewRate(rRange.Next(0, RangeChangeCurrencyRate),rChance.Next(0, 100));                    
                    debug+=" "+ ListAllDevice[dev].GetAvailablesCurrency[cur].GetRate.ToString();
                    
                }
            }

            if (GetGraph?.GetGraphDisplay)
            {
                GetGraph.GetGraphDisplay.GetComponent<ListCurrencyDisplay>().Prime(GetCurentDevice.GetAvailablesCurrency);
            }
            
            
        }

        IEnumerator TimeChangeCurrenceRate()
        {
            float t = Timeleft;
            while (true)
            {
                Timeleft = (float)Math.Round(t);
                if (t > 0)
                {
                    t -= 1 * Time.deltaTime / 1f;               
                }
                else
                {
                    ChangeRateCurrency(GetShop.GetListAllDevices);

                    nextTimeChange = timeChangeInterval + DateTime.Now.TimeOfDay;

                    Save.dataChangeCurrencyRate = new int[]
                    {
                    DateTime.Now.Year,
                    DateTime.Now.Month,
                    DateTime.Now.Day,
                    nextTimeChange.Hours,
                    nextTimeChange.Minutes,
                    nextTimeChange.Seconds,
                    nextTimeChange.Milliseconds,

                    };
                    t = (float)(nextTimeChange - DateTime.Now.TimeOfDay).TotalSeconds; 
                }
                
                
                yield return 0;
            }
            
        }


        public void LoadGame()
        {
            Save = SaveManager.LoadGameData();
            GetIncome.GetBalance = (decimal)Save.Coin;
            GetIncome.GetDonatBalance = Save.DonatCoin;
        }



#if UNITY_ANDROID && !UNITY_EDITOR
        private void OnApplicationPause(bool pause)
    {
        if(pause){
           Save.NameCurentDevice = GetCurentDevice.GetNameDevice;
            Save.NameCurentCurrency = GetCurentCurrency.GetNameCurrency;
            Save.AllDev = new string[GetShop.GetListAllDevices.Count];
            Save.ItemAllDevice = new string[GetShop.GetListAllDevices.Count];
            for (int i = 0; i < GetShop.GetListAllDevices.Count; i++)
            {
                Save.AllDev[i] += GetShop.GetListAllDevices[i].GetIncomePerClickDevice.ToString();
                Save.AllDev[i] += "|";
                Save.AllDev[i] += GetShop.GetListAllDevices[i].GetIncomePerSecondDevice.ToString();
                Save.AllDev[i] += "|";
                Save.AllDev[i] += GetShop.GetListAllDevices[i].GetPurchased.ToString();

                for (int k = 0; k < GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems.Count; k++)
                {
                    Save.ItemAllDevice[i] += GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetCoastItem.ToString();
                    Save.ItemAllDevice[i] += "*";
                    Save.ItemAllDevice[i] += GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetDonatCoastItem.ToString();
                    Save.ItemAllDevice[i] += "*";
                    Save.ItemAllDevice[i] += GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetClickForce.ToString();
                    Save.ItemAllDevice[i] += "*";
                    Save.ItemAllDevice[i] += GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetSecondForce.ToString();
                    Save.ItemAllDevice[i] += "*";
                    Save.ItemAllDevice[i] += GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetProcentUpgrade.ToString();
                    Save.ItemAllDevice[i] += "*";
                    Save.ItemAllDevice[i] += GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetChanceDestroy.ToString();
                    if (k != GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems.Count - 1)
                        Save.ItemAllDevice[i] += "|";
                }
            }
            Save.Coin = (float)GetIncome.GetBalance;
            Save.DonatCoin = GetIncome.GetDonatBalance;
            Save.clicksTaskProgress = GetPlayerTasksManager.clicksTaskProgress;
            Save.lastTaskIsDone = GetPlayerTasksManager.lastTaskIsDone;
            Save.previewsClickTask = GetPlayerTasksManager.previewsClickTask;
            Save.modeAds = GetAdvertManager.offAds;
            SaveManager.SaveGameData(Save);   
    }
    }
#endif
        public void OnApplicationQuit()
        {

            //savegame
            Save.NameCurentDevice = GetCurentDevice.GetNameDevice;
            Save.NameCurentCurrency = GetCurentCurrency.GetNameCurrency;
            Save.AllDev = new string[GetShop.GetListAllDevices.Count];
            Save.ItemAllDevice = new string[GetShop.GetListAllDevices.Count];
            Save.CurrencysRateAllDevice = new string[GetShop.GetListAllDevices.Count];
            for (int i = 0; i < GetShop.GetListAllDevices.Count; i++)
            {
                Save.AllDev[i] += GetShop.GetListAllDevices[i].GetIncomePerClickDevice.ToString();
                Save.AllDev[i] += "|";
                Save.AllDev[i] += GetShop.GetListAllDevices[i].GetIncomePerSecondDevice.ToString();
                Save.AllDev[i] += "|";
                Save.AllDev[i] += GetShop.GetListAllDevices[i].GetPurchased.ToString();

                for (int k = 0; k < GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems.Count; k++)
                {
                    Save.ItemAllDevice[i] += GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetCoastItem.ToString();
                    Save.ItemAllDevice[i] += "*";
                    Save.ItemAllDevice[i] += GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetDonatCoastItem.ToString();
                    Save.ItemAllDevice[i] += "*";
                    Save.ItemAllDevice[i] += GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetClickForce.ToString();
                    Save.ItemAllDevice[i] += "*";
                    Save.ItemAllDevice[i] += GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetSecondForce.ToString();
                    Save.ItemAllDevice[i] += "*";
                    Save.ItemAllDevice[i] += GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetProcentUpgrade.ToString();
                    Save.ItemAllDevice[i] += "*";
                    Save.ItemAllDevice[i] += GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems[k].GetChanceDestroy.ToString();
                    if (k != GetShop.GetListAllDevices[i].GetAvailablesUpgradeItems.Count - 1)
                        Save.ItemAllDevice[i] += "|";
                }

                for (int k = 0; k < GetShop.GetListAllDevices[i].GetAvailablesCurrency.Count; k++)
                {
                    Save.CurrencysRateAllDevice[i] += GetShop.GetListAllDevices[i].GetAvailablesCurrency[k].GetRate.ToString();
                    if (k != GetShop.GetListAllDevices[i].GetAvailablesCurrency.Count - 1)
                        Save.CurrencysRateAllDevice[i] += "|";
                }
            }

            if (GetTaskManager.CurentTask != null)
                Save.CurrentTask = GetTaskManager.CurentTask.NameTask;
            else Save.CurrentTask = null;
            

            for (int i = 0; i < GetTaskManager.GetSavedTasks.Count; i++)
            {
                for (int k = 0; k < GetTaskManager.GetAllTask.Count; k++)
                {
                    if (string.Equals(GetTaskManager.GetSavedTasks[i].NameTask, GetTaskManager.GetAllTask[k].NameTask))
                    {
                        GetTaskManager.GetSavedTasks[i].target = GetTaskManager.GetAllTask[k].target;
                        GetTaskManager.GetSavedTasks[i].reward = GetTaskManager.GetAllTask[k].reward;
                        GetTaskManager.GetSavedTasks[i].progress = GetTaskManager.GetAllTask[k].progress;
                        GetTaskManager.GetSavedTasks[i].readyFA = GetTaskManager.GetAllTask[k].readyFA;
                    }
                }
            }

            Save.AllTasks = new string[GetTaskManager.GetSavedTasks.Count];
            for (int i = 0; i < Save.AllTasks.Length; i++)
            {
                Save.AllTasks[i] += GetTaskManager.GetSavedTasks[i].NameTask;
                Save.AllTasks[i] += "|";
                Save.AllTasks[i] += GetTaskManager.GetSavedTasks[i].target.ToString();
                Save.AllTasks[i] += "|";
                Save.AllTasks[i] += GetTaskManager.GetSavedTasks[i].progress.ToString();
                Save.AllTasks[i] += "|";
                Save.AllTasks[i] += GetTaskManager.GetSavedTasks[i].reward.ToString();
                Save.AllTasks[i] += "|";
                Save.AllTasks[i] += GetTaskManager.GetSavedTasks[i].readyFA.ToString();
            } 
            
            Save.Coin = (float)GetIncome.GetBalance;
            Save.DonatCoin = GetIncome.GetDonatBalance;            
            Save.modeAds = GetAdvertManager.offAds;
            SaveManager.SaveGameData(Save);
        }
    }


}