﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Manager.GameManager;

public class Currency : MonoBehaviour
{
    #region Fields
    [SerializeField]
    string NameCurrency;
    [SerializeField]
    float CuretnRate;
    [SerializeField]
    float DefaultRate;
    float Fluctuations = 10;
    [SerializeField]
    Color ColorInGraph;
    [SerializeField]
    GameObject PrefabPoint;
    int maxRateRange;
    #endregion
    
    #region Internals

    public string GetNameCurrency
    {
        get => NameCurrency;

        private set => NameCurrency = value;
    }

    public float GetRate
    {
        get => CuretnRate;
        set
        {
            if (value > 0)
            {
                CuretnRate = value;
            }
        }
    }

    public Color GetColor { get => ColorInGraph; private set => ColorInGraph = value; }

    public GameObject GetPoint { get => PrefabPoint; private set => PrefabPoint = value; }
    public CurrencyDisplay GetCurrencyDisplay { get; set; }

    public float GetMinRate { get; private set; }
    public float GetMaxRate { get; private set ; }


    #endregion

    public void CalculatemaxRateRange()
    {
        maxRateRange = (int)DefaultRate + (GetGameManager.GetRangeChangeCurrencyRate * 2);
    }

    public void CalculatedFluctuations()
    {
        
        GetMinRate = ((CuretnRate/2) - 100) - Fluctuations;
        GetMaxRate = ((CuretnRate/2) - 100) + Fluctuations;
    }


    public void CalculateNewRate(int Range,int Chance)
    {
        
        //Course fall
        if (Chance<=30)
        {
            if (CuretnRate - Range <= 0)
            {
                CuretnRate = 1;
            }
            else
            {
                CuretnRate -= Range;
            }
           
        }

        //Takeoff course
        if (Chance>=31 && Chance <= 60)
        {
            if (CuretnRate + Range >= maxRateRange)
            {
                CuretnRate = maxRateRange;
            }
            else CuretnRate += Range;           
        }

    }
    

}
