﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using static Manager.GameManager;

//Ui отображение валют в списке.
public class CurrencyDisplay : MonoBehaviour {

    Currency Currency;
    [Header("Main information")]   
    [SerializeField]
    Text DisplayNameCurrency;
    [SerializeField]
    Image ArrowUp, ArrowDown;

    public Image GetUpRateImage { get => ArrowUp; }
    public Image GetDownRateImage { get => ArrowDown; }

    public void SelectCurrency()
    {
        //currency change in graph
        GetGraph.SwitchCurentCurrency(Currency);
    }

    public void Prime(Currency CurrencyPrefab)
    {
        Currency = CurrencyPrefab;
        //Set display currency values
        if (DisplayNameCurrency != null)
        {
            DisplayNameCurrency.text = CurrencyPrefab.GetNameCurrency;
            DisplayNameCurrency.color = CurrencyPrefab.GetColor;
        } 

    }
}
