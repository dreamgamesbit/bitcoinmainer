﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Отображение и создание UI префабов списка валют 
public class ListCurrencyDisplay : MonoBehaviour {
    [SerializeField]
    RectTransform TargetTransform;
    [SerializeField]
    CurrencyDisplay CurrencyDisplay;
    

    List<CurrencyDisplay> LCurrencyDisplay = new List<CurrencyDisplay>();

    public void Prime(List<Currency> CurrencyList)
    {
        //delite old list currency in chart
        if (TargetTransform.childCount > 0)
        {
            for (int i = 0; i < TargetTransform.childCount; i++)
            {
                Destroy(TargetTransform.GetChild(i).gameObject);
            }
        }

        List<Currency> BufferCurrenceList = new List<Currency>();

        for (int i = 0; i < CurrencyList.Count; i++)
        {
            BufferCurrenceList.Add(new Currency());
            BufferCurrenceList[i] = CurrencyList[i];
        }

        Currency maxRateCurrency = BufferCurrenceList[0];

        //Create list currency in chart
        for (int i = 0; i < CurrencyList.Count; i++)
        {
            
            LCurrencyDisplay.Add(new CurrencyDisplay());
            LCurrencyDisplay[i] = Instantiate(CurrencyDisplay);
            LCurrencyDisplay[i].transform.SetParent(TargetTransform,false);

            for (int k = 0; k < BufferCurrenceList.Count; k++)
            {
                if (BufferCurrenceList[k].GetRate > maxRateCurrency.GetRate && BufferCurrenceList.Count > 1 )
                {
                    maxRateCurrency = BufferCurrenceList[k];                    
                    
                }
                else maxRateCurrency = BufferCurrenceList[0];
            }


            LCurrencyDisplay[i].Prime(maxRateCurrency);
            maxRateCurrency.GetCurrencyDisplay = LCurrencyDisplay[i];

            BufferCurrenceList.Remove(maxRateCurrency);

        }

        
    }
}
