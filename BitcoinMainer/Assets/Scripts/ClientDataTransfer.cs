﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;

public class ClientDataTransfer : MonoBehaviour
{

    PlayerConfig config;
    public void GetData()
    {
        config = new PlayerConfig(ServerCommands.Login);
        GameClient.StartClient(config);
    }

    public void ShowCurrencies()
    {
        foreach (KeyValuePair<string, float> currency in GameClient.GetCurrencyData())
        {
            Debug.Log($"{currency}");
        }
    }

    public Dictionary<string, float> GetCurrencies()
    {
        return GameClient.GetCurrencyData();
    }

    public int GetTimeToChange()
    {
        return GameClient.GetTimeToChange();
    }

}

public enum ServerCommands
{
    Login = 1,
    Register = 2,
    GetActualCurrencyData = 3
}

public class PlayerConfig
{
    public string login;
    public string password;
    public Dictionary<string, float> currencyData = new Dictionary<string, float>() { { "", 0f } };
    public int timeToChange = 0;
    public string errorMessage = "";
    public string command = "";

    public bool wrongLogin { get; set; } = false;
    public bool wrongPass { get; set; } = false;
    public bool wrongCommand { get; set; } = false;
    public bool hasUpdate { get; set; } = false;

    /// <summary>
    /// Recommended command to use for the duration of the disabled authorization: "ServerCommands.GetActualCurrencyData". But you can use any ServerCommand.
    /// </summary>
    /// <returns></returns>
    public PlayerConfig(ServerCommands command)
    {
        /*
        if (isPlayerNameAllowed(playerLogin))
        {
            login = playerLogin;
        }
        else throw new ArgumentException("Playername can contains only letters, numbers, special characters \".\",\"_\",\"-\", can't be empty, less than 4 and more than 15 symbols!");

        if (isPlayerPasswordAllowed(playerPassword))
        {
            password = playerPassword;
        }
        else throw new ArgumentException("Password cannot be empty, less than 4 and more than 18 symbols!");
        */

        login = "player";
        password = "12345";
        switch (command)
        {
            case ServerCommands.Login: this.command = "login"; break;
            case ServerCommands.Register: this.command = "register"; break;
            case ServerCommands.GetActualCurrencyData: this.command = "getactdata"; break;
            default: throw new InvalidEnumArgumentException(); 
        }
    }

    public bool IsWrongLogin() => wrongLogin;

    public bool IsWrongPass() => wrongPass;

    public bool IsWrongCommand() => wrongCommand;

    public bool HasDataUpdate() => hasUpdate;

    public Dictionary<string, float> GetCurrencyData() => currencyData;

    public void GetCurrenciesData(ref Dictionary<string, float> dictionary) => dictionary = currencyData;
}


public static class GameClient
{

    private static string host = "68.183.102.0";
    private static int port = 80;
    private static PlayerConfig client = new PlayerConfig(ServerCommands.GetActualCurrencyData);

    /// <summary>
    /// You can set host and port for connect via "connectHost=" and "connectPort=" variations..
    /// </summary>
    /// <returns></returns>

    public static void StartClient(PlayerConfig playerConfig, string connectHost = "68.183.102.0", int connectPort = 80)
    {
        byte[] bytes = new byte[2048];

        host = connectHost;
        port = connectPort;
        //host = "localhost";
        //port = 8000;


        client = playerConfig;
        try
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(host);
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

            Socket senderSocket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            senderSocket.ReceiveTimeout = 4000;
            senderSocket.SendTimeout = 4000;

            try
            {
                // Connect to remote server.
                senderSocket.Connect(remoteEP);

                // Get data as json string from client object.
                string json = GetJsonPlayerInfo(client);

                // Convert data to bytes.
                byte[] data = Encoding.UTF8.GetBytes(json);

                // Send the data through the socket.
                int bytesSent = senderSocket.Send(data);

                // Receive data from remote server.
                int bytesRec = senderSocket.Receive(bytes);

                json = Encoding.UTF8.GetString(bytes);
                client = GetPlayerInfoFromJson(json);

            }
            catch (ArgumentNullException ane)
            {
                UnityEngine.Debug.Log($"ArgumentNullException : {ane.ToString()}");
            }
            catch (SocketException se)
            {
                UnityEngine.Debug.Log($"SocketException : {se.ToString()}");
            }
            catch (Exception e)
            {
                UnityEngine.Debug.Log($"Unexpected exception : {e.ToString()}");
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    /// <summary>
    /// Method return currency data if data is not empty, else method return null.
    /// </summary>
    /// <returns></returns>
    public static Dictionary<string, float> GetCurrencyData()
    {
        Dictionary<string, float> result;
        if (client.currencyData.Count != 0)
        {
            result = client.currencyData;
        }
        else result = null;

        return result;
    }

    public static int GetTimeToChange()
    {
        return client.timeToChange;
    }

    public static string GetJsonPlayerInfo(PlayerConfig playerConfig)
    {
        string result = JsonConvert.SerializeObject(playerConfig);
        return result;
    }

    public static PlayerConfig GetPlayerInfoFromJson(string json)
    {
        PlayerConfig result = JsonConvert.DeserializeObject<PlayerConfig>(json);
        return result;
    }

}
